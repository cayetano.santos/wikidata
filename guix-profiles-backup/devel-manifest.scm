;; Ce fichier « manifeste » peut être passé à « guix package -m » pour reproduire
;; le contenu de votre profil. Son contenu est « symbolique » : il ne spécifie que les
;; noms des paquets. Pour reproduire exactement le même profil, vous devez aussi
;; retenir les canaux utilisés, ceux renvoyés par « guix describe ».
;; Voir la section « Répliquer Guix » dans le manuel.

(specifications->manifest
  (list "python-pygments"
        "python-jupyter-console"
        "python-notebook"
        "readline@8.0"
        "bear"
        "julia"
        "python@3.8"
        "cpplint"
        "global"
        "clang:extra"
        "clang-toolchain"
        "gcc-toolchain"
        "universal-ctags"
        "cuda-toolkit"
        "cudnn"
        "make"))
