---
format: Org
categories: emacs config outshine navy outorg
toc: yes
title: csb-outshine
content: emacs outshine /navy / outorg mode config file
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#outshine][Outshine]]
  - [[#variables][Variables]]
  - [[#keys][Keys]]
  - [[#misc][Misc]]
  - [[#_references_][_References_]]
- [[#outorg][Outorg]]
- [[#navi][Navi]]
  - [[#hooks][Hooks]]
- [[#trailer][Trailer]]

* Introduction

- What

  Outshine is a collection of libraries (outshine, outorg and navi) implementing convenience
  navigation functionality on top of outline-minor-mode

- How

  Org headings preceded by the major mode comment item

- Use

  Toggle it [[file:init.el::%3B%3B%20**%20Global%20-%20Toggle%20Map][with]] C-x t o

- More

  [[http://orgmode.org/worg/org-tutorials/org-outside-org.html#screencasts][Outshine @ worg]]

- Keys

  Defined [[file:init.el::(define-key%20endless/toggle-map%20(kbd%20"o")%20(lambda%20()%20(interactive)][here]]

  + M-#       ->  outorg-edit-as-org          :: enter org mode
  + C-c M-s   ->  navi-switch-to-twin-buffer  :: switch to toc buffer
  + C-c M-t   ->  navi-search-and-switch      :: create a new toc buffer

  once in an org buffer with outorg

  + M-#      ->  outorg-save-edits-to-tmp-file :: save and exit
  + C-x C-s  ->  outorg-copy-edits-and-exit    :: save temp buffer

* Header

#+begin_src emacs-lisp
  ;; package --- Summary
  ;;
  ;;; Commentary:
  ;;
  ;; This file contains ...
  ;;
  ;;; Code:
#+end_src

* Outline

Derived from text mode.

Here I define a major mode dependent action after moving around.

#+begin_src emacs-lisp
  (advice-add 'outline-next-visible-heading :after
              (lambda (arg)
                (csb/generic-after-jump-advice 'outline)))

  (advice-add 'outline-previous-visible-heading :after
              (lambda (arg)
                (csb/generic-after-jump-advice 'outline)))

  (advice-add 'outline-forward-same-level :after
              (lambda (arg)
                (csb/generic-after-jump-advice 'outline)))

  (advice-add 'outline-backward-same-level :after
              (lambda (arg)
                (csb/generic-after-jump-advice 'outline)))

  (advice-add 'outline-up-heading :after
              (lambda (arg)
                (csb/generic-after-jump-advice 'outline)))
#+end_src

Hard set headings faces

Use same faces as the one in org.

#+begin_src emacs-lisp :tangle no
  ;; Headings in outline (outshine) mode
  ;; (custom-theme-set-faces 'user
  ;;                         `(outline-1 ((t (:inherit org-level-1 :font ,csb/font-prog-mode
  ;;                                                   :height ,csb/font-prog-mode-size))))
  ;;                         `(outline-2 ((t (:inherit org-level-2 :font ,csb/font-prog-mode
  ;;                                                   :height ,(- csb/font-prog-mode-size 5)))))
  ;;                         `(outline-3 ((t (:inherit org-level-3 :font ,csb/font-prog-mode
  ;;                                                   :height ,(- csb/font-prog-mode-size 10)))))
  ;;                         `(outline-4 ((t (:inherit org-level-4 :font ,csb/font-prog-mode
  ;;                                                   :height ,(- csb/font-prog-mode-size 15)))))
  ;;                         `(outline-5 ((t (:inherit org-level-5 :font ,csb/font-prog-mode
  ;;                                                   :height ,(-csb/font-prog-mode-size
  ;;                                                        20))))))
  (if (member (and (daemonp) (daemonp)) csb/theme-zenburn-server)
      ;; When using Zenburn
      (custom-theme-set-faces 'user
                              `(outline-1 ((t (:weight medium :height 1.25 :family "Symbola"))))
                              `(outline-2 ((t (:weight medium :height 1.22 :family "Symbola"))))
                              `(outline-3 ((t (:weight medium :height 1.20 :family "Symbola"))))
                              `(outline-4 ((t (:weight medium :height 1.20 :family "Symbola"))))
                              `(outline-5 ((t (:weight medium :height 1.20 :family "Symbola"))))
                              `(outline-6 ((t (:weight medium :height 1.20 :family "Symbola"))))
                              ;; `(org-level-8 ((t (,@headline ,@variable-tuple))))
                              ;; `(org-level-7 ((t (,@headline ,@variable-tuple))))
                              ;; `(org-level-6 ((t (,@headline ,@variable-tuple))))
                              ;; `(org-level-5 ((t (,@headline ,@variable-tuple))))
                              ;; `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.2))))
                              ;; `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.3))))
                              ;; `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.35))))
                              ;; `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.4))))
                              ;; `(org-document-title ((t (,@headline
                              ;;                              ,@variable-tuple :height 1.5 :weight bold :underline nil))))
                              )
    ;; Otherwise
    (custom-theme-set-faces 'user
                            `(outline-1 ((t (:inherit org-level-1 :height 1.1 :family "Symbola"))))
                            `(outline-2 ((t (:inherit org-level-2 :height 1.05 :family "Symbola"))))
                            `(outline-3 ((t (:inherit org-level-3 :height 1.05 :family "Symbola"))))
                            `(outline-4 ((t (:inherit org-level-4 :height 1.05 :family "Symbola"))))
                            `(outline-5 ((t (:inherit org-level-5 :height 1.05 :family "Symbola"))))))

#+end_src

#+begin_src emacs-lisp :tangle (if csb/perso-config (format "%scsb-outshine.el" org-config-path) "no")
  (custom-theme-set-faces 'user
                          `(outline-1 ((t (:inherit org-level-1 :height 1.1 :family "Symbola"))))
                          `(outline-2 ((t (:inherit org-level-2 :height 1.05 :family "Symbola"))))
                          `(outline-3 ((t (:inherit org-level-3 :height 1.05 :family "Symbola"))))
                          `(outline-4 ((t (:inherit org-level-4 :height 1.05 :family "Symbola"))))
                          `(outline-5 ((t (:inherit org-level-5 :height 1.05 :family "Symbola")))))
#+end_src

* Outshine

What -> Gives an org appearance to programming mode buffers

_References_

 - [[https://github.com/tj64/outshine][Outshine @ github]]
 - [[http://www.modernemacs.com/post/outline-ivy/][Managing code with Outlines]]

#+begin_src emacs-lisp
  (eval-when-compile
    (require 'outshine))
#+end_src

** Variables

#+begin_src emacs-lisp
  (setq outshine-use-speed-commands t
        outshine-startup-folded-p nil)

  ;; (defvar outshine-next-recenter 10 "toto")
  ;; ;; (setq outshine-next-recenter 10)
  ;; (defvar outshine-prev-recenter 10 "toto")
  ;; ;; (setq outshine-prev-recenter 10)
#+end_src

** Keys

Note that several outline functions are adviced to perform custom functionality after jumping.

I remap outline-minor-mode-map to mirror org-mode.

 - Retrieve usual orgmode =S-TAB= and =C-RET= behaviour

#+begin_src emacs-lisp
  (define-key outline-minor-mode-map (kbd "<backtab>") 'outshine-cycle-buffer)
  (define-key outline-minor-mode-map (kbd "C-RET") 'outshine-insert-heading)
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "<M-up>")
   (outline-move-subtree-up 1)
   (outline-on-heading-p))
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "<M-down>")
   (outline-move-subtree-down 1)
   (outline-on-heading-p))
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "<M-left>")
   (outline-promote 1)
   (outline-on-heading-p))
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "<M-right>")
   (outline-demote 1)
   (outline-on-heading-p))
#+end_src

Outside of headings, I want this behaviour too

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-x n s")
   (progn
     (outline-previous-visible-heading 1)
     (outshine-narrow-to-subtree))
   t)
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-c C-n")
   (outline-next-visible-heading 1)
   t)
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-c C-p")
   (outline-previous-visible-heading 1)
   t)
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-c C-f")
   (outline-forward-same-level 1)
   (outline-on-heading-p))
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-c C-b")
   (outline-backward-same-level 1)
   (outline-on-heading-p))
#+end_src

#+begin_src emacs-lisp
  (outshine-define-key-with-fallback
   outshine-mode-map (kbd "C-c C-u")
   (outline-up-heading 1)
   (outline-on-heading-p))
#+end_src

** Misc

As per [[https://github.com/tj64/outshine/issues/38][#38]], I need to add this line, otherwise outshine breaks company idle completion

#+begin_src emacs-lisp :tangle no
  (add-to-list 'company-begin-commands 'outshine-self-insert-command)
#+end_src

** Fix issue

I need to advice this function to make it work with indented headings.

#+begin_src emacs-lisp :tangle no
  (defadvice outshine-calc-outline-level (around titi activate)
    ;;(require 'cl)
    (flet ((outshine-calc-outline-regexp () outline-regexp))
      ad-do-it))
#+end_src

* Outorg

What  -> Reverts org-babel
How   -> Create a new temp org buffer with original contents (comments and code) reverted

References:

[[https://github.com/tj64/outorg][Outorg @ github]]

#+begin_src emacs-lisp :tangle no
  (eval-after-load 'outorg '(progn
                              (define-key outorg-edit-minor-mode-map "\C-x\C-s" 'outorg-save-edits-to-tmp-file)
                              (define-key outorg-edit-minor-mode-map "\M-#" 'outorg-copy-edits-and-exit)))
  (global-set-key (kbd "M-#") 'outorg-edit-as-org)    ;; remove defaults
#+end_src

* Navi

What  -> Implements a twin Table of Contens (TOC) buffer to navigate the origin buffer.

References:

[[https://github.com/tj64/navi][navi @ github]]

#+begin_src emacs-lisp :tangle no
    (require 'navi-mode)
    (global-set-key (kbd "M-s M-s") nil)  ;; remove defaults
    (global-set-key (kbd "M-s n") nil)    ;; remove defaults
    ;;(eval-after-load 'navi-mode '(progn
                                   ;; ;; Follow
                                   ;; (define-key navi-mode-map "l" 'occur-mode-display-occurrence)  ;; go to the location and let point in toc buffer
                                   ;; (define-key navi-mode-map "h" 'occur-mode-goto-occurrence)     ;; go to the location and move point to the original buffer
                                   ;; ;; go to the location and delete other windows
                                   ;; (define-key navi-mode-map (kbd "RET") (lambda() (interactive) (occur-mode-goto-occurrence)(delete-other-windows)))

                                   ;; ;; Quit
                                   ;; (define-key navi-mode-map "Q" (lambda() (interactive) (kill-this-buffer)(delete-window)))
                                   ;; (define-key navi-mode-map "q" (lambda() (interactive) (navi-quit-and-switch)(delete-window)))

                                   ;; ;; help
                                   ;; (define-key navi-mode-map "?" 'navi-show-help)

                                   ;; ;; Scrolling
                                   ;; (define-key navi-mode-map (kbd "SPC") 'scroll-up-command)
                                   ;; (define-key navi-mode-map (kbd "S-SPC") 'scroll-down-command)

                                   ;; (define-key navi-mode-map "d" nil)
                                   ;; (define-key navi-mode-map (kbd "DEL") nil)
                                   ;; (define-key navi-mode-map (kbd "C-o") nil)
  ;;))
#+end_src

** Hooks

#+begin_src emacs-lisp :tangle no
  (add-hook 'navi-mode-hook (lambda ()
                                 ;; Follow
                                 (define-key navi-mode-map "l" 'occur-mode-display-occurrence)  ;; go to the location and let point in toc buffer
                                 (define-key navi-mode-map "h" 'occur-mode-goto-occurrence)     ;; go to the location and move point to the original buffer
                                 ;; go to the location and delete other windows
                                 (define-key navi-mode-map (kbd "RET") (lambda() (interactive) (occur-mode-goto-occurrence)(delete-other-windows)))

                                 ;; Quit
                                 (define-key navi-mode-map "Q" (lambda() (interactive) (kill-this-buffer)(delete-window)))
                                 (define-key navi-mode-map "q" (lambda() (interactive) (navi-quit-and-switch)(delete-window)))

                                 ;; help
                                 (define-key navi-mode-map "?" 'navi-show-help)

                                 ;; Scrolling
                                 (define-key navi-mode-map (kbd "SPC") 'scroll-up-command)
                                 (define-key navi-mode-map (kbd "S-SPC") 'scroll-down-command)

                                 (define-key navi-mode-map "d" nil)
                                 (define-key navi-mode-map (kbd "DEL") nil)
                                 (define-key navi-mode-map (kbd "C-o") nil)))
#+end_src

* Trailer

#+begin_src emacs-lisp
  (provide 'csb-outshine)

  ;;; csb-outshine.el ends here
#+end_src
