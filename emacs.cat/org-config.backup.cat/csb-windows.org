---
format: Org
categories:
toc: yes
title: csb-windows
content: emacs config windows
...

Library of rutines for window management

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#packages][Packages]]
  - [[#windmove][Windmove]]
  - [[#buffer-move][Buffer-move]]
  - [[#tiling][Tiling]]
  - [[#transpose-frame][Transpose-frame]]
  - [[#ace-window][Ace-window]]
  - [[#windcycle][Windcycle]]
  - [[#window-numbering][Window-numbering]]
  - [[#golden-ratio][Golden Ratio]]
  - [[#winner-mode][Winner-mode]]
  - [[#switch-window][Switch-window]]
  - [[#window][Window]]
  - [[#transpose-frame--c-o-z][Transpose-frame  C-o z]]
- [[#dflksj][dflksj]]
  - [[#grid-layout][Grid Layout]]
    - [[#example-of-use][Example of use]]
  - [[#rotate][Rotate]]
- [[#hydra][Hydra]]
- [[#killing----------c-o-qqo1][Killing          C-o {qQo1}]]
- [[#enlarge--shrink-c---updownrightleft][Enlarge / Shrink C - {up;down;right;left}]]
  - [[#macros][Macros]]
  - [[#keys][Keys]]
- [[#splitting--------c-o-_][Splitting        C-o {_;|}]]
  - [[#grid-layout-1][Grid Layout]]
    - [[#example-of-use-1][Example of use]]
  - [[#config][Config]]
- [[#trailer][Trailer]]

* Introduction

Code related to window management functionality within one Emacs frame.

Switching windows with

- C-o s (switch-window)
- C-o ' ('ace-window)
- C-o o (other-window)
- window numbering : Alt-N

http://www.emacswiki.org/emacs/CategoryWindows

TODO: convert to minor mode ?

* Header

#+begin_src emacs-lisp
;; package --- Summary
;;
;;; Commentary:
;;
;; This file contains ...
;;
;;; Code:
#+end_src

* Packages

#+begin_src emacs-lisp
  (eval-when-compile
    (add-to-list 'load-path "~/.emacs.d/libraries/tiling")
    (require 'tiling)
    (require 'windmove)
    (require 'buffer-move))
#+end_src

** Windmove

Move point to neighbour window. Build into emacs.

#+begin_src emacs-lisp :tangle no
  (use-package windmove
    :ensure nil
    :config
    (setq windmove-wrap-around nil))
#+end_src

** Buffer-move

Move buffers around.

#+begin_src emacs-lisp
  (use-package buffer-move
    :config
    (setq buffer-move-behavior 'swap))
#+end_src

** Tiling

Tiling window management under emacs, provided by el-get.

Set a layout:

  C-u 0   -> master left
  C-u 1   -> master top
  C-u 2   -> master even horizontal
  C-u 3   -> master even vertical
  C-u 4   -> tile 4

Cycle layouts:

  C-o SPC -> cycle layouts

#+begin_src emacs-lisp

  ;; Get package if not available
  ;; (let ((csb/dest-dir "~/.emacs.d/libraries/tiling"))
  ;;   (unless (file-exists-p csb/dest-dir)
  ;;     (shell-command (format "git clone https://github.com/lgfang/tiling.git %s" csb/dest-dir)))
  ;;   )

  (add-to-list 'load-path "~/.emacs.d/libraries/tiling")
  (require 'tiling)

  (defvar csb/windows-current-tiling nil "tile currently in use")

  (defun csb/set-tile (&optional arg)
    (interactive "P")
    (if (= arg 5)
        (setq csb/windows-current-tiling 'left)
      (let ((bufs (mapcar 'window-buffer (window-list nil -1 nil))))
        (if (> (length bufs) 1)
            (cond ((= arg 0)
                   (funcall 'tiling-master-left bufs)
                   (setq csb/windows-current-tiling 'left))
                  ((= arg 1)
                   (funcall 'tiling-master-top bufs)
                   (setq csb/windows-current-tiling 'up))
                  ((= arg 2)
                   (funcall 'tiling-even-horizontal bufs)
                   (setq csb/windows-current-tiling 'horizontal))
                  ((= arg 3)
                   (funcall 'tiling-even-vertical bufs)
                   (setq csb/windows-current-tiling 'vertical))
                  ((and (= arg 4) (= (length bufs) 4))
                   (funcall 'tiling-tile-4 bufs)
                   (setq csb/windows-current-tiling '4))
                  ((and (= arg 4) (not (= (length bufs) 4)))
                   (message (format "wrong number of windows %i, must be 4." (length bufs)))))
          (message "wrong number of windows for setting up a layout")))))
#+end_src

** Transpose-frame

Installed with el-get, the package is online [[http://www.emacswiki.org/emacs/TransposeFrame][here]] and [[https://github.com/emacsmirror/transpose-frame][here]], and locally [[file:~/.emacs.d/el-get/transpose-frame/transpose-frame.el::%3B%3B%3B%20Commentary:][here]].

** Windcycle

#+begin_src emacs-lisp :tangle no
  (require 'windcycle)
#+end_src

#+begin_src emacs-lisp :tangle no
  (define-key window-map (kbd "s k") 'buffer-up-swap)
  (define-key window-map (kbd "s j") 'buffer-down-swap)
  (define-key window-map (kbd "s l") 'buffer-right-swap)
  (define-key window-map (kbd "s h") 'buffer-left-swap)
  (global-set-key (kbd "C-x <up>") 'windmove-up-cycle)
  (global-set-key (kbd "C-x <down>") 'windmove-down-cycle)
  (global-set-key (kbd "C-x <right>") 'windmove-right-cycle)
  (global-set-key (kbd "C-x <left>") 'windmove-left-cycle)
  (global-set-key (kbd "M-<up>") 'windmove-up-cycle)
  (global-set-key (kbd "M-<down>") 'windmove-down-cycle)
  (global-set-key (kbd "M-<right>") 'windmove-right-cycle)
  (global-set-key (kbd "M-<left>") 'windmove-left-cycle)
#+end_src

** Window-numbering

Select target window with alt-n

#+begin_src emacs-lisp :tangle no
  (use-package window-numbering
    :init (window-numbering-mode 1)
    :config
    (setq window-numbering-auto-assign-0-to-minibuffer t)
    ;; (define-key window-numbering-keymap (kbd "C-o 1") 'select-window-1)
    ;; (define-key window-numbering-keymap (kbd "C-o 2") 'select-window-2)
    ;; (define-key window-numbering-keymap (kbd "C-o 3") 'select-window-3)
    ;; (define-key window-numbering-keymap (kbd "C-o 4") 'select-window-4)
    )

  ;; (when (and (string= (system-name) "apcnb158")
  ;;            (or
  ;;             (string= (and (daemonp) (daemonp)) "default")
  ;;             (string= (and (daemonp) (daemonp)) "erc")))

  ;;   )
#+end_src

** Golden Ratio

#+begin_src emacs-lisp :tangle no
  (golden-ratio-mode -1)
  (defun pl/helm-alive-p ()
    (if (boundp 'helm-alive-p)
        (symbol-value 'helm-alive-p)))
  (add-to-list 'golden-ratio-inhibit-functions 'pl/helm-alive-p)
  (defun pl/guide-key-alive-p ()
      (when (string= major-mode "guide-key") t))
  (add-to-list 'golden-ratio-inhibit-functions 'pl/guide-key-alive-p)
#+end_src

** Winner-mode

#+begin_src emacs-lisp
  (use-package winner
    :ensure nil
    :init
    (winner-mode 1))
#+end_src

** Switch-window

Jump to a window by labeling them.

#+begin_src emacs-lisp
  (use-package switch-window
    :config
    (setq  switch-window-qwerty-shortcuts '("f" "j" "g" "h" "k" "d" "u")
           switch-window-shortcut-style 'qwerty ;; quail
           switch-window-timeout 2
           switch-window-increase 5
           switch-window-relative t
           switch-window-threshold 2))
#+end_src

** Window

Built in [[file:/usr/share/emacs/25.0.50/lisp/window.el.gz::%3B%3B%3B%20window.el%20---%20GNU%20Emacs%20window%20commands%20aside%20from%20those%20written%20in%20C][here.]]

** Transpose-frame  C-o z

To visualize the effect see [[file:~/.emacs.d/el-get/transpose-frame/transpose-frame.el::%3B%3B%3B%20Commentary:][this]].

#+begin_src emacs-lisp :tangle no
  (define-key window-map (kbd "z t") 'transpose-frame)            ;; Swap x-direction and y-direction
  (define-key window-map (kbd "z t") 'flip-frame)                 ;; flip vertically
  (define-key window-map (kbd "z t") 'flop-frame)                 ;; flip horizontally
  (define-key window-map (kbd "z t") 'rotate-frame)               ;; Rotate 180 degrees
  (define-key window-map (kbd "z t") 'rotate-frame-clockwise)     ;; Rotate 90 degrees clockwise
  (define-key window-map (kbd "z t") 'rotate-frame-anticlockwise) ;; Rotate 90 degrees anti-clockwise
#+end_src

* dflksj

** Grid Layout

Based on [[http://www.emacswiki.org/emacs/GridLayout][emacs wiki article]]

Split the current frame into a grid of X columns and Y rows

#+begin_src emacs-lisp :tangle no
  (defun split-window-multiple-ways (x y)
    "Split the current frame into a grid of X columns and Y rows."
    (interactive "nColumns: \nnRows: ")
    ;; one window
    (delete-other-windows)
    (dotimes (i (1- x))
        (split-window-horizontally)
        (dotimes (j (1- y))
          (split-window-vertically))
        (other-window y))
    (dotimes (j (1- y))
      (split-window-vertically))
    (balance-windows))
#+end_src

Find Xth horizontal and Yth vertical window from top-left of FRAME

#+begin_src emacs-lisp :tangle no
  (defun get-window-in-frame (x y &optional frame)
    "Find Xth horizontal and Yth vertical window from top-left of FRAME."
    (let ((orig-x x) (orig-y y)
          (w (frame-first-window frame)))
      (while (and (windowp w) (> x 0))
        (setq w (windmove-find-other-window 'right 1 w)
              x (1- x)))
      (while (and (windowp w) (> y 0))
        (setq w (windmove-find-other-window 'down 1 w)
              y (1- y)))
      (unless (windowp w)
        (error "No window at (%d, %d)" orig-x orig-y))
      w))
#+end_src

Set Xth horizontal and Yth vertical window to BUFFER from top-left of FRAME

#+begin_src emacs-lisp :tangle no
  (defun set-window-buffer-in-frame (x y buffer &optional frame)
    "Set Xth horizontal and Yth vertical window to BUFFER from top-left of FRAME."
    (set-window-buffer (get-window-in-frame x y frame) buffer))
#+end_src

Fill all windows of the current frame with buffers using major-mode MODE

#+begin_src emacs-lisp :tangle no
(defun show-buffers-with-major-mode (mode)
  "Fill all windows of the current frame with buffers using major-mode MODE."
  (interactive
   (let* ((modes (cl-loop for buf being the buffers
                              collect (symbol-name (with-current-buffer buf
                                                           major-mode)))))
     (list (intern (completing-read "Mode: " modes)))))
  (let ((buffers (cl-loop for buf being the buffers
                              when (eq mode (with-current-buffer buf
                                                     major-mode))
                                     collect buf)))
    (dolist (win (window-list))
      (when buffers
        ;; (show-buffer win (car buffers))
        (set-window-buffer win (car buffers))
        (setq buffers (cdr buffers))))))
#+end_src

*** Example of use

#+begin_src emacs-lisp :tangle no
  (erc)
  (info)
  (calendar)
  (ielm)
  (defun my-reset-windows ()
    "Bring back 3x3 window configuration with my favorite buffers."
    (interactive)
    (split-window-multiple-ways 3 3)
    (set-window-buffer-in-frame 0 0 (get-buffer "*scratch*"))
    (set-window-buffer-in-frame 1 0 (find-file-noselect "~/.emacs"))
    (set-window-buffer-in-frame 2 0 (get-buffer "#emacs"))
    (set-window-buffer-in-frame 0 1 (get-buffer "*info*"))
    (set-window-buffer-in-frame 1 1 (get-buffer "*Calendar*"))
    (set-window-buffer-in-frame 2 1 (dired "~/"))
    (set-window-buffer-in-frame 0 2 (eshell))
    (set-window-buffer-in-frame 1 2 (get-buffer "*Messages*"))
    (set-window-buffer-in-frame 2 2 (get-buffer "*ielm*")))
  (my-reset-windows)
#+end_src

** Rotate

#+begin_src emacs-lisp
  (defun rotate-windows ()
    "Rotate your windows"
    (interactive)
    (cond
     ((not (> (count-windows) 1))
      (message "You can't rotate a single window!"))
     (t
      (let ((i 0)
            (num-windows (count-windows)))
        (while  (< i (- num-windows 1))
          (let* ((w1 (elt (window-list) i))
                 (w2 (elt (window-list) (% (+ i 1) num-windows)))
                 (b1 (window-buffer w1))
                 (b2 (window-buffer w2))
                 (s1 (window-start w1))
                 (s2 (window-start w2)))
            (save-excursion
              (set-window-buffer w1 b2))
            (when (string= (persp-name (persp-curr)) "erc")
              (goto-char (point-max)))
            (save-excursion
              (set-window-buffer w2 b1))
            (when (string= (persp-name (persp-curr)) "erc")
              (goto-char (point-max)))
            (save-excursion
              (set-window-start w1 s2))
            (when (string= (persp-name (persp-curr)) "erc")
              (goto-char (point-max)))
            (save-excursion
              (set-window-start w2 s1))
            (when (string= (persp-name (persp-curr)) "erc")
              (goto-char (point-max)))
            (setq i (1+ i))))))))
#+end_src

* Hydra

Post function helper function

#+begin_src emacs-lisp
  (defun csb/window-post()
    ;; (if csb/windows-current-tiling
    ;;     (cond
    ;;      ((eq csb/windows-current-tiling 'left)
    ;;       (balance-windows)
    ;;       ;;(fit-window-to-buffer)
    ;;       )
    ;;      ((eq csb/windows-current-tiling 'up)
    ;;       (balance-windows))
    ;;      ((eq csb/windows-current-tiling 'vertical)
    ;;       (balance-windows))
    ;;      ((eq csb/windows-current-tiling 'horizontal)
    ;;       (balance-windows)
    ;;       ;;(fit-window-to-buffer)
    ;;       )
    ;;      ((eq csb/windows-current-tiling '4)
    ;;       (balance-windows)
    ;;       ;;(fit-window-to-buffer)
    ;;       ))
    ;;   ;;(fit-window-to-buffer)
    ;;   nil
    ;;   )
    ;;(when csb/fit-window-enable
    ;;  (fit-window-to-buffer))
    ;; (recenter-top-bottom (if (and (string= major-mode "vhdl-tools-mode")
    ;;                               (boundp 'vhdl-tools-recenter-nb-lines))
    ;;                          vhdl-tools-recenter-nb-lines
    ;;                        '(4)))
    (beacon-blink)
    (let ((csb/window-fringes-enable t))
      (csb/window-fringes))
    ;; (when (string= (persp-name (persp-curr)) "erc")
    ;;   (goto-char (point-max)))
    )
#+end_src

#+begin_src emacs-lisp
  (setq csb/hydra-window-timeout 3.0)
  (setq csb/hydra-window-idle 1.0)
#+end_src

#+begin_src emacs-lisp
  (defun csb/windows-other-window (&optional arg)
    (interactive "P")
    (if (= (count-windows) 2)
        (other-window 1)
      (ace-window arg))
    (csb/window-post))
#+end_src

#+begin_src emacs-lisp
  (defhydra csb/hydra-window
    (:color red
            :pre (progn
                   (require 'switch-window)
                   ;; (csb/org-require 'csb-fringes t)
                   (setq csb/window-fringes-enable nil)
                   (require 'tiling)
                   (require 'windmove)
                   (require 'buffer-move))
            :idle csb/hydra-window-idle
            :post (progn
                    (setq csb/window-fringes-enable t))
            :timeout csb/hydra-window-timeout)
    "
                      ^^Point^^  ^^Window^^   ^^Buffer^^  ^Winner^   ^Resize^     ^Split^       ^Misc^       ^Tiling^         ^Resize^         ^Kill^
                     ---------------------------------------------------------------------------------------------------------------------------------
                        ^^^^        ^^    ^^     ^^    ^^     ^ ^   shr_i_nk    ^  ^          SPC rotate   _t4_ 2x2
                        ^_k_^      ^_wk_^       ^_xk_^      _U_    _=_balance  _\__ vertical     _s_witch   _tr_ cycle         _rk_   ↑     _q_  kill other buffer
                       _h_ _l_   _wh_  _wl_   _xh_  _xl_    _D_      _a_rea    _\|_ horizontal   _o_ther    _tl_ master left   _rj_   ↓     _Q_  kill other buffer - delete window
                        ^_j_^      ^_wj_^       ^_xj_^      ^ ^    mi_n_imize  ^  ^             _y_ext     _tu_ master up     _rl_ ←| |→   _0_  delete window
                        ^^^^        ^^    ^^     ^^    ^^     ^ ^  _m_aximize  ^  ^             _p_rev     _th_ horizontal    _rh_ |↦ ↤|   _1_  delete other windows
                        ^^^^        ^^    ^^     ^^    ^^     ^ ^    _f_it          ^^         ^’^ ace     _tv_ vertical      _ç_ swap
                        ^^^^        ^^    ^^     ^^    ^^     ^ ^    ^ ^            ^^         ^^             _t0_ none
        "
    ;; point
    ("j"  (progn (call-interactively 'windmove-down)(csb/window-post)) nil)
    ("k"  (progn (call-interactively 'windmove-up)(csb/window-post)) nil)
    ("h"  (progn (call-interactively 'windmove-left)(csb/window-post)) nil)
    ("l"  (progn (call-interactively 'windmove-right)(csb/window-post)) nil)
    ;;
    ("xj" (progn (buf-move-down)(csb/window-post)) nil)
    ("xk" (progn (buf-move-up)(csb/window-post)) nil)
    ("xh" (progn (buf-move-left)(csb/window-post)) nil)
    ("xl" (progn (buf-move-right)(csb/window-post)) nil)
    ;;
    ("wj" tiling-tile-down nil)
    ("wk" tiling-tile-up nil)
    ("wh" tiling-tile-left nil)
    ("wl" tiling-tile-right nil)
    ;;
    ("tr" tiling-cycle nil)
    ("tl" (progn (csb/set-tile 0)(csb/window-post)) nil :exit t)
    ("tu" (progn (csb/set-tile 1)(csb/window-post)) nil :exit t)
    ("th" (progn (csb/set-tile 2)(csb/window-post)) nil :exit t)
    ("tv" (progn (csb/set-tile 3)(csb/window-post)) nil :exit t)
    ("t4" (csb/set-tile 4) nil :exit t)
    ("t0" (csb/set-tile 5) nil :exit t)
    ;;
    ("U" (progn
           (winner-undo)
           (setq this-command 'winner-undo)) nil)
    ("D" winner-redo nil)
    ("s" switch-window nil :exit t)
    ("’" ace-window nil :exit t)
    ("ç" ace-swap-window nil)
    ;; "window resize"
    ("=" balance-windows nil :exit t)
    ("a" balance-windows-area nil :exit t)
    ("n" (progn
           (window-configuration-to-register :csb/window)
           (minimize-window)
           (other-window 1)) nil :exit t)
    ("m" (progn
           (window-configuration-to-register :csb/window)
           (maximize-window)) nil :exit t)
    ("b" (progn
           (jump-to-register :csb/window)) nil :exit t)
    ;;
    ("y" select-next-window nil)
    ("p" select-previous-window nil)
    ("o" csb/windows-other-window nil :exit t)
    ;;
    ("rk" my-enlarge-window-vertically nil)
    ("rl" my-enlarge-window-horizontally nil)
    ("rj" my-shrink-window-vertically nil)
    ("rh" my-shrink-window-horizontally nil)
    ;;
    ("z" nil :exit t)
    ("i" shrink-window-if-larger-than-buffer nil :exit t)
    ("f" fit-window-to-buffer nil :exit t)
    ("SPC" rotate-windows nil :color red :timeout csb/hydra-window-timeout)
    ;; splitting
    ("_"  (lambda ()
            (interactive)
            (split-window-below)
            ;; (hide-mode-line-mode -1)
            (call-interactively 'windmove-down)
            (csb/window-post)) nil)
    ("|"  (lambda ()
            (interactive)
            (split-window-right)
            (call-interactively 'windmove-right)
            (csb/window-post)) nil)
    ;;
    ("q" (progn (save-excursion
                  (other-window 1)
                  (kill-this-buffer)
                  (other-window 1))) nil)
    ("Q" (progn (save-excursion
                  (other-window 1)
                  (kill-this-buffer)
                  (delete-window)
                  (other-window 1))) nil :exit t)
    ("0" delete-window nil :exit t)
    ("1" (lambda ()
           (interactive)
           (delete-other-windows)
           ;; (hide-mode-line-mode t)
           (csb/window-post)) nil :exit t))
#+end_src

#+begin_src emacs-lisp
  (global-set-key (kbd "C-x q") 'kill-this-buffer)
  ;; save-buffers-kill-terminal)
  (global-set-key (kbd "C-x Q") (lambda()(interactive)(kill-buffer-and-close)))
#+end_src

#+begin_src emacs-lisp
(defun select-next-window ()
  "Switch to the next window"
  (interactive)
  (select-window (next-window)))

(defun select-previous-window ()
  "Switch to the previous window"
  (interactive)
  (select-window (previous-window)))
#+end_src

* Killing          C-o {qQo1}

Kills {current / other} {buffer / window}

#+begin_src emacs-lisp
  (defun kill-other-buffer ()
    "Kill other buffer."
    (interactive)
    (save-excursion
      (other-window 1)
      (kill-this-buffer)
      (other-window 1)))

  (defun kill-buffer-and-close ()
    "Kill other buffer."
    (interactive)
    (save-excursion
      (kill-this-buffer)
      (delete-window)))
#+end_src

* Enlarge / Shrink C - {up;down;right;left}

** Macros

#+begin_src emacs-lisp
  (defun my-enlarge-window-vertically ()
    (interactive)
    (enlarge-window 3))

  (defun my-enlarge-window-horizontally ()
    (interactive)
    (enlarge-window-horizontally 3))

  (defun my-shrink-window-vertically ()
    (interactive)
    (shrink-window 3))

  (defun my-shrink-window-horizontally ()
    (interactive)
    (shrink-window-horizontally 3))
#+end_src

** Keys

In window mode

#+begin_src emacs-lisp :tangle no
  (define-key window-map (kbd "<up>")    (make-repeatable-command 'my-enlarge-window-vertically))
  (define-key window-map (kbd "<right>") (make-repeatable-command 'my-enlarge-window-horizontally))
  (define-key window-map (kbd "<down>")  (make-repeatable-command 'my-shrink-window-vertically))
  (define-key window-map (kbd "<left>")  (make-repeatable-command 'my-shrink-window-horizontally))
#+end_src

In terminal

#+begin_src emacs-lisp :tangle no
  (define-key window-map "\M-[1;5A" (make-repeatable-command 'my-enlarge-window-vertically))
  (define-key window-map "\M-[1;5C" (make-repeatable-command 'my-enlarge-window-horizontally))
  (define-key window-map "\M-[1;5B" (make-repeatable-command 'my-shrink-window-vertically))
  (define-key window-map "\M-[1;5D" (make-repeatable-command 'my-shrink-window-horizontally))
#+end_src

* Splitting        C-o {_;|}

Remove defaults

#+begin_src emacs-lisp
  (global-unset-key (kbd "C-x 2"))
  (global-unset-key (kbd "C-x 3"))
  (global-unset-key (kbd "C-x 0"))
  (global-unset-key (kbd "C-x 1"))
  (global-unset-key (kbd "C-x o"))
  (global-unset-key (kbd "C-x |"))
  (global-unset-key (kbd "C-x _"))
#+end_src

** Grid Layout

Based on [[http://www.emacswiki.org/emacs/GridLayout][emacs wiki article]]

Split the current frame into a grid of X columns and Y rows

#+begin_src emacs-lisp
  (defun split-window-multiple-ways (x y)
    "Split the current frame into a grid of X columns and Y rows."
    (interactive "nColumns: \nnRows: ")
    ;; one window
    (delete-other-windows)
    (dotimes (i (1- x))
        (split-window-horizontally)
        (dotimes (j (1- y))
          (split-window-vertically))
        (other-window y))
    (dotimes (j (1- y))
      (split-window-vertically))
    (balance-windows))
#+end_src

Find Xth horizontal and Yth vertical window from top-left of FRAME

#+begin_src emacs-lisp
  (defun get-window-in-frame (x y &optional frame)
    "Find Xth horizontal and Yth vertical window from top-left of FRAME."
    (let ((orig-x x) (orig-y y)
          (w (frame-first-window frame)))
      (while (and (windowp w) (> x 0))
        (setq w (windmove-find-other-window 'right 1 w)
              x (1- x)))
      (while (and (windowp w) (> y 0))
        (setq w (windmove-find-other-window 'down 1 w)
              y (1- y)))
      (unless (windowp w)
        (error "No window at (%d, %d)" orig-x orig-y))
      w))
#+end_src

Set Xth horizontal and Yth vertical window to BUFFER from top-left of FRAME

#+begin_src emacs-lisp
  (defun set-window-buffer-in-frame (x y buffer &optional frame)
    "Set Xth horizontal and Yth vertical window to BUFFER from top-left of FRAME."
    (set-window-buffer (get-window-in-frame x y frame) buffer))
#+end_src

Fill all windows of the current frame with buffers using major-mode MODE

#+begin_src emacs-lisp
(defun show-buffers-with-major-mode (mode)
  "Fill all windows of the current frame with buffers using major-mode MODE."
  (interactive
   (let* ((modes (cl-loop for buf being the buffers
                              collect (symbol-name (with-current-buffer buf
                                                           major-mode)))))
     (list (intern (completing-read "Mode: " modes)))))
  (let ((buffers (cl-loop for buf being the buffers
                              when (eq mode (with-current-buffer buf
                                                     major-mode))
                                     collect buf)))
    (dolist (win (window-list))
      (when buffers
        ;; (show-buffer win (car buffers))
        (set-window-buffer win (car buffers))
        (setq buffers (cdr buffers))))))
#+end_src

*** Example of use

#+begin_src emacs-lisp :tangle no
  (erc)
  (info)
  (calendar)
  (ielm)
  (defun my-reset-windows ()
    "Bring back 3x3 window configuration with my favorite buffers."
    (interactive)
    (split-window-multiple-ways 3 3)
    (set-window-buffer-in-frame 0 0 (get-buffer "*scratch*"))
    (set-window-buffer-in-frame 1 0 (find-file-noselect "~/.emacs"))
    (set-window-buffer-in-frame 2 0 (get-buffer "#emacs"))
    (set-window-buffer-in-frame 0 1 (get-buffer "*info*"))
    (set-window-buffer-in-frame 1 1 (get-buffer "*Calendar*"))
    (set-window-buffer-in-frame 2 1 (dired "~/"))
    (set-window-buffer-in-frame 0 2 (eshell))
    (set-window-buffer-in-frame 1 2 (get-buffer "*Messages*"))
    (set-window-buffer-in-frame 2 2 (get-buffer "*ielm*")))
  (my-reset-windows)
#+end_src

** Config

#+begin_src emacs-lisp
  (with-eval-after-load 'csb-windows

    (defun find-windows-config-file-other ()
      "Edit the windows-mode config file, in another window."
      (interactive)
      (find-file-other-window (concat org-config-path "csb-windows.page")))

    (defun find-windows-config-file ()
      "Edit the windows-mode config file."
      (interactive)
      (find-file (concat org-config-path "csb-windows.page"))))
#+end_src

* Trailer

#+begin_src emacs-lisp
  (defvar csb/csb-windows 'loaded "module is loaded successfully")

  (message (format "Loaded csb-windows !"))

  (provide 'csb-windows)

  ;;; csb-windows.el ends here
#+end_src
