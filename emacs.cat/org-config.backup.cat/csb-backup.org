---
format: Org
categories: emacs config backup
toc: yes
title: csb-backup
content: emacs backup config file
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#header][Header]]
- [[#introduction][Introduction]]
- [[#auto-save][Auto Save]]
- [[#super-save][Super-save]]
- [[#desktop-save][Desktop Save]]
- [[#savehist][Savehist]]
- [[#saveplace][Saveplace]]
- [[#auto-backup][Auto Backup]]
- [[#trailer][Trailer]]

* Header

#+begin_src emacs-lisp
;;; csb-backup --- Summary
;;
;;; Commentary:
;;
;; This file contains ...
#+end_src

* Introduction

* Auto Save

Auto-saves a file every few seconds or every few characters. saves a copy of a file every so often
while you are editing it. If some catastrophe caused you to close Emacs or shut down your machine
without saving the file then you can use M-x recover-file to recover the file from its auto save. By
default, the auto save files are saved in the same directory as the original file, and are given a
name of the form #file#. The auto-save files have names like #foo# and are deleted automatically
when you manually save a file.

_When you save a file, the auto save file is deleted_.

#+begin_src emacs-lisp :tangle no
  (defvar autosave-dir
    (if running-os-is-linux
        (expand-file-name ".autosaves/" user-emacs-directory)
      (expand-file-name (expand-file-name ".autosaves/"
                                          user-emacs-directory))))

  (setq auto-save-default t ;; do auto-saving of every file-visiting buffer.
        auto-save-list-file-prefix autosave-dir
        ;; auto-save-file-name-transforms `((".*" ,autosave-dir t))
        ;; auto-save-file-name-transforms
        ;;  `(("\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'" ,(concat autosave-dir "\\2") t))
        delete-auto-save-files t ;; delete auto-save file when a buffer is saved or killed.
        auto-save-timeout 30     ;; Number of seconds idle time before auto-save.
        auto-save-interval 300)  ;; Number of input events between auto-saves.
#+end_src

_References_

 - http://nicolas-petton.fr/blog/buffer-watcher.html
 - [[http://emacsredux.com/blog/2013/05/09/keep-backup-and-auto-save-files-out-of-the-way/][Keep Backup and Auto-save Files Out of the Way]]

* Super-save

A minor mode that saves your Emacs buffers when they lose focus or emacs is idle.

#+begin_src emacs-lisp
  (use-package super-save
    :init
    (super-save-mode 1)
    :config
    (add-to-list 'super-save-triggers 'tab-previous)
    (add-to-list 'super-save-triggers 'tab-next)
    (add-to-list 'super-save-triggers 'csb/windows)
    (setq super-save-auto-save-when-idle t ;; Save current buffer automatically when Emacs is idle
          super-save-idle-duration 5
          super-save-remote-files nil))    ;; Save remote files when t, ignore them otherwise
#+end_src

_References_

 - http://emacsredux.com/blog/2016/01/30/super-save/
 - [[https://github.com/bbatsov/super-save][github]]
 - [[mu4e:msgid:yu1sh1ld3mo.fsf@apc.univ-paris7.fr][Re: Production tests JEUDI 4 Oct. 11h]]

* Desktop Save

When Desktop Save mode is enabled, the state of Emacs is saved from
one session to another.

#+begin_src emacs-lisp :tangle no
  (use-package desktop
    :init (desktop-save-mode t)
    :config
    (setq desktop-auto-save-timeout 10)
    (setq desktop-path (list (concat user-emacs-directory)))
    (setq desktop-dirname (list (concat user-emacs-directory)))
    (setq desktop-base-file-name ".emacs.desktop")
    ;; (setq desktop-modes-not-to-save '(python-mode))
    (setq desktop-load-locked-desktop t)
    (setq desktop-restore-frames t)
    (setq desktop-restore-in-current-display t)
    (setq desktop-restore-reuses-frames t)
    (setq desktop-restore-forces-onscreen t))

  ;; (setq desktop-globals-to-save nil)
  ;; (setq desktop-locals-to-save nil)
  ;; (defadvice desktop-save-buffer-p (before desktop-save-buffer-p-test (filename bufname mode &rest _dummy) activate)
  ;;   (setq CurrentPerspBufferNames_ (mapcar (lambda (l) (buffer-name l) ) (persp-buffers persp-curr)))
  ;;   (setq CurrentPerspBufferNames (remove-if-not 'identity CurrentPerspBufferNames_)) ;; remove nils
  ;;   ;; if buffer is not a member of the list, change its name to nil
  ;;   (if (not (member (symbol-name (make-symbol bufname)) CurrentPerspBufferNames))
  ;;    (setq filename nil)))
  ;; (ad-unadvise 'desktop-save-buffer-p)
#+end_src

_References_

- http://ergoemacs.org/emacs/emacs_save_restore_opened_files.html
- https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html

* Savehist

When Savehist mode is enabled, minibuffer history is saved periodically and when
exiting Emacs. When Savehist mode is enabled for the first time in an Emacs
session, it loads the previous minibuffer history from ‘savehist-file’.

#+begin_src emacs-lisp
  (use-package savehist
    :ensure nil
    :init
    (savehist-mode t)
    :config
    (add-to-list 'savehist-additional-variables 'kill-ring)
    (setq savehist-file
          (if (daemonp)
              (expand-file-name
               (format ".savehist/savehist-%s"
                       (daemonp))
               user-emacs-directory)
            (expand-file-name ".savehist/.savehist-nodaemon" user-emacs-directory))))
#+end_src

* Saveplace

When you visit a file, point goes to the last place where it was when you
previously visited the same file.

#+begin_src emacs-lisp
  (use-package saveplace
    :ensure nil
    :init
    (save-place-mode t)
    :config
    (setq save-place-file
          (if (daemonp)
              (expand-file-name
               (format ".saveplace/saved-places-%s" (daemonp))
               user-emacs-directory)
            (expand-file-name ".saveplace/saved-places-nodaemon" user-emacs-directory))))
#+end_src

* Auto Backup

By default the backup file is made in the same directory as the original with a
name like file~. The way the backup works is that Emacs makes a copy of a file
the first time you save it in an Emacs session. It only makes that one backup
though, so this is not very useful if you keep your session running for a long
time and want to recover an earlier version of a file.

#+begin_src emacs-lisp
  (setq make-backup-files t
        ;; store all backup in local backups dir
        backup-directory-alist '((".*" . ".backups"))
        delete-old-versions t     ; don't ask about deleting old versions
        vc-make-backup-files t    ; backup version controlled files
        kept-new-versions 20      ; keep 10 latest versions
        kept-old-versions 20
        backup-by-copying t       ; don't clobber symlinks
        version-control t)        ; number backups
#+end_src

_References_

 - [[info:emacs#Backup][Backup@info]]
 - [[http://emacsredux.com/blog/2013/05/09/keep-backup-and-auto-save-files-out-of-the-way/][Keep Backup and Auto-save Files Out of the Way]]
 - [[elfeed:http://planet.emacsen.org/atom.xml#http://pragmaticemacs.com/?p=528][Pragmatic Emacs: Auto Save and Backup Every Save]]

* Trailer

#+begin_src emacs-lisp
  (defvar csb/csb-backup 'loaded "module is loaded successfully")

  (provide 'csb-backup)

  ;;; csb-backup.el ends here
#+end_src
