---
format: Org
categories: software mail
toc: yes
title: Mail
content: mail management
...

* Introduction

Here I depict contact management, including saving of contact information from
incoming mail and mail address retrieval from the list of contacts, including
automated querying.

* Address book management

** nottoomuch

First, I proceed to implement address query with the help of the
[[https://github.com/domo141/nottoomuch/blob/master/nottoomuch-addresses.rst][nottoomuch-addresses.sh]] [[file:/software.cat/mail.cat/mail::*Scripts][script]]. As explained [[http://notmuchmail.org/emacstips/#index13h2][here]], this bash script may be used
as a =notmuch= database address 'juicer', building-up its own data base of mail
addresses. For doing so, I need this line in my =muttrc=.

#+begin_src
  set query_command="source ~/.mutt/query_contacts '%s' $my_account"
#+end_src

First, note that when I launch mutt using the zsh [[file:/software.cat/zsh.cat/zsh-plugin-mutt::*Introduction][plugin]], I export several env
variables following the current account, instructing =notmuch= to use a given
account maildir. Namely, I declare the current =notmuch= config file with
=$NOTMUCH_CONFIG=. This is used for =nottoomuch=.

The script =query_commands= is as follows

#+begin_src sh
  #!/bin/bash
  a=`lbdbq $1 2>/dev/null`
  $HOME/.mutt/notmuch-new.sh $2 new
  XDG_CONFIG_HOME=$HOME/Maildir/$2/.addresses
  junk=`[[ ! -d $XDG_CONFIG_HOME/nottoomuch ]] && \
    $HOME/.mutt/nottoomuch/nottoomuch-addresses.sh --rebuild 2>&1`
  junk=`$HOME/.mutt/nottoomuch/nottoomuch-addresses.sh --update 2>&1`
  b=`$HOME/.mutt/nottoomuch/nottoomuch-addresses.sh $1`
  c="----nottoomuch----"
  c1="   "
  echo -e "$a\n$c1\n$c\n$c1\n$b"
#+end_src

Here, I temporally define the place where =nottoomuch= will store the address
database, under =Maildir/account=, with help of =$XDG_CONFIG_HOME=. Then, I
rebuild the database if it is non existing and I update it. Finally, I query the
db.

I must be noted that in case you want to rebuild the database from scratch, you
have to add =--rebuild= after =--update=. This is necessary too before issuing
any updating command. Refer to =nottoomuch-addresses.sh --help= for details.

With the previous, when I redact a new mail, addresses are looked up in the full
history of incoming mails, which may amount to a lot of data. New mail senders,
are automatically indexed by =notmuch=, and so they are by
=nottoomuch-adresses.sh=, which makes them available for new mails. But this is
not exactly address book management. A proper contact db would be more useful.

** Contacts

In order to build-up a contacts agenda I proceed following the logic developed and detailed in
previous sections. I am using now a different macro which saves sender information in a contacts
agenda.

#+begin_src
  macro index,pager a "|~/.mutt/save_contacts\n" "add the sender address to contacts book"
#+end_src

Its operation is similar to the one described in the previous =annotate macro= section. It pipes
the current mail contents to the =save_contactcs= bash script. From there, I am calling the
=build_contacts= emacs macro.

#+begin_src elisp
  (defun build_contacts()
      (setq my-org-protocol-flag t)
      (if (buffer-exists "emacs-pipe")
          (kill-buffer "emacs-pipe")
        )
      (find-file "/tmp/emacs-pipe")
      (let
          (
           (name (or (car (mail-extract-address-components (or (mail-fetch-field "from") " "))) "%^{name}"))
           (email_pro (or (car (cdr (mail-extract-address-components (or (mail-fetch-field "from") " ")))) "%^{email pro}"))
           (email_per (or (car (cdr (mail-extract-address-components (or (mail-fetch-field "from_per") " ")))) "%^{email perso}"))
           )
        (org-capture nil "c")))
#+end_src

Same discussion as before applies: I capture the contents of some variables into a custom
template. When no information can be automatically extracted from the parsed mail, the routine
prompts the user for the requested information.

#+begin_src elisp
          ("c" "Contacts" entry (file "~/.mutt/contacts.org")
         "* %(eval 'Name)
:PROPERTIES:
:FULL_NAME: %(eval 'Name)
:EMAIL: %(eval 'Email_pro)
:EMAIL_PRO: %(eval 'Email_pro)
:EMAIL_PERSO: %(eval 'Email_per)
:EMAIL_OTHER:
:PHONE:
:PHONE_PRO:
:PHONE_PERSO:
:PHONE_OTHER:
:ADDRESS:
:ADDRESS_PRO:
:ADDRESS_PERSO:
:ADDRESS_OTHER:
:BIRTHDAY:
:ALIAS:
:NICKNAME:
:IGNORE:
:ICON:
:NOTE:
:END:"
         :empty-lines 1)
#+end_src

** Org contacts

Right here, I have a simple way to construct my contact list. Now, it would be great to link this
to mutt using, for example, the great [[http://julien.danjou.info/projects/emacs-packages#org-contacts][org-contacts.el]] announced [[http://lists.gnu.org/archive/html/emacs-orgmode/2011-02/msg00443.html][here]]. The idea is being able to parse my
=contacts.org= file searching for mail addresses. As the previous template is =org-contacts= compatible, I may
use the [[http://www.spinnaker.de/lbdb/][little brother data base]] program for this task. This utility reads several different formats, providing
the right address, as explained [[http://jasonwryan.com/blog/2012/04/21/lbdb/][in this article]].

Now, the point is how to read a =org-contacts= formatted =contacts.org= file. There exist a special module for
this, as explained [[http://lists.gnu.org/archive/html/emacs-orgmode/2011-10/msg01059.html][here]]. It consist on a perl script doing the conversion work. Now, by using this line

#+begin_src
  set query_command="lbdbq '%s' 2>/dev/null"
#+end_src

in the =muttrc= main configuration file, the work is done. I am using =lbdb= instead of =nottoomuch= now. But, what
if I needed to used both? For example, I might be using the former for close contacts and the later
for loose contacts. In this case, I need to develop a simple bash script in =query_contacts= to combine
both. Finally, I use

#+begin_src
  set query_command="source ~/.mutt/query_contacts '%s'"
#+end_src

to query both sources of contacts. The principle may be extended to contacts from =abook=, etc.
