---
format: Org
categories:
toc: yes
title:
content:
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:


- [[#addons][Addons]]
  - [[#security][Security]]
    - [[#tracking][Tracking]]
    - [[#exexceptions][ExExCeptions]]
- [[#searching][Searching]]

* Addons

** List of add-ons

Correcteur de texte - Language Tool
Dark Mode
Facebook Container
Github dark theme
Grammalecte fr
Hide scrollbars
Https partout
Instapaper
Instaread
Mendeley Web Importer
PassFF
Pinboard+
Privacy Badger
Refined Github
uBlock Origin
Vimium
Wikiwand webextension

** Navigating

*** Vimium

**** Exluded urls and keys
:PROPERTIES:
:header-args: :tangle no
:END:

_Patterns_

#+begin_src sh
  https?://mail.google.com/*
#+end_src

**** Custom search engines
:PROPERTIES:
:header-args: :tangle search.vimium :mkdirp yes :tangle-mode (identity #o444)
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

When modify this, refer to the corresponding wiki page instead and update it too !!

ref: search wiki page

https://github.com/philc/vimium/wiki/Search-Engines
https://github.com/philc/vimium/wiki/Search-Completion

_Qwant_

#+begin_src sh
  qen: https://www.qwant.com/?r=GB&sr=en&l=en_gb&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant en
  qfr: https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant fr
  qes: https://www.qwant.com/?r=ES&sr=es&l=es_es&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant es


  qlen: https://lite.qwant.com/?l=en&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite en
  qles: https://lite.qwant.com/?l=es&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite es
  qlfr: https://lite.qwant.com/?l=fr&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite fr
#+end_src

_ArchWiki_

#+begin_src sh
  aw https://wiki.archlinux.org/index.php?search=%s ArchWiki
#+end_src

_Wikipedia_

#+begin_src sh
  wen: https://en.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia en
  wes: https://es.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia es
  wfr: https://fr.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia es
#+end_src

_Google_

#+begin_src sh
  gen: https://www.google.com/search?q=%s Google en
  ges: https://www.google.es/search?q=%s Google es
  gfr: https://www.google.fr/search?q=%s Google fr

  gm: https://www.google.com/maps?q=%s Google maps
  # l: https://www.google.com/search?q=%s&btnI I'm feeling lucky...
  # l: https://www.google.com/search?btnI&q=%s I'm feeling lucky...
#+end_src

_Youtube_

#+begin_src sh
  y: https://www.youtube.com/results?search_query=%s Youtube
#+end_src

_DuckDuckGo_

#+begin_src sh
  # d: https://duckduckgo.com/?q=%s DuckDuckGo
  d: https://duckduckgo.com/?ia=about&q=%s DuckDuckGo
#+end_src

_Amazon_

#+begin_src sh
  # az: https://www.amazon.com/s/?field-keywords=%s Amazon
  az: http://www.amazon.co.uk/s/?field-keywords=%s Amazon
#+end_src

_Dictionary (Merriam-Webster)._

#+begin_src sh
  # dw: http://www.merriam-webster.com/dictionary/%s Merriam-Webster
  # b: https://www.bing.com/search?q=%s Bing
#+end_src

**** Custom key mappings
:PROPERTIES:
:header-args: :tangle keys.vimium :mkdirp yes :tangle-mode (identity #o444)
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

When modify this, refer to the corresponding wiki page instead and update it too !!
See wiki page

_Search_

https://github.com/philc/vimium/wiki/Tips-and-Tricks#keyboard-shortcuts-for-custom-search-engines

#+begin_src sh
  map sq Vomnibar.activate keyword=q
  map sQ Vomnibar.activateInNewTab keyword=q
  map sg Vomnibar.activate keyword=g
  map sG Vomnibar.activateInNewTab keyword=g
#+end_src

#+begin_src sh
  map <a-n> nextTab
  map <a-p> previousTab
  map d removeTab
  map u restoreTab
#+end_src

#+begin_src sh
  map <c-e> Vomnibar.activateEditUrl
  map <c-E> activateEditUrlInNewTab
#+end_src

 - Unmap defaults

   #+begin_src sh
     unmap ge
     unmap gE
     unmap x
     unmap X
     unmap J
     unmap gt
     unmap gT
     unmap K
     unmap J
     unmap <c-y>
   #+end_src

   #+begin_src sh
  zH              Scroll all the way to the left (scrollToLeft)
  zL              Scroll all the way to the right (scrollToRight)

  yy              Copy the current URL to the clipboard (copyCurrentUrl)
  p               Open the clipboard's URL in the current tab (openCopiedUrlInCurrentTab)
  P               Open the clipboard's URL in a new tab (openCopiedUrlInNewTab)

  v               Enter visual mode (enterVisualMode)
  V               Enter visual line mode (enterVisualLineMode)

  Pass the next key to the page (passNextKey)
  gi              Focus the first text input on the page (focusInput)
  f               Open a link in the current tab (LinkHints.activateMode)
  F               Open a link in a new tab (LinkHints.activateModeToOpenInNewTab)
  Open a link in a new tab & switch to it (LinkHints.activateModeToOpenInNewForegroundTab)
  <a-f>           Open multiple links in a new tab (LinkHints.activateModeWithQueue)
  Download link url (LinkHints.activateModeToDownloadLink)
  Open a link in incognito window (LinkHints.activateModeToOpenIncognito)
  yf              Copy a link URL to the clipboard (LinkHints.activateModeToCopyLinkUrl)
  [[              Follow the link labeled previous or < (goPrevious)
   ]]               Follow the link labeled next or > (goNext)
  gf              Select the next frame on the page (nextFrame)
  gF              Select the page's main/top frame (mainFrame)
  m               Create a new mark (Marks.activateCreateMode)
  `               Go to a mark (Marks.activateGotoMode)
#+end_src

 - Miscellaneous

  ?  :: Show help (showHelp)
  gs :: View page source (toggleViewSource)

 - Navigating history

  H  :: Go back in history (goBack)
  L  :: Go forward in history (goForward)

 - Manipulating tabs

  t               Create new tab (createTab)

  ^               Go to previously-visited tab (visitPreviousTab)
  g0              Go to the first tab (firstTab)
  g$              Go to the last tab (lastTab)
  yt              Duplicate current tab (duplicateTab)
  Pin or unpin current tab (togglePinTab)
  <a-m>           Mute or unmute current tab (toggleMuteTab)

  X               Restore closed tab (restoreTab)
  W               Move tab to new window (moveTabToNewWindow)
  Close tabs on the left (closeTabsOnLeft)
  Close tabs on the right (closeTabsOnRight)
  Close all other tabs (closeOtherTabs)
  <<              Move tab to the left (moveTabLeft)
  >>              Move tab to the right (moveTabRight)

 - Using the vomnibar

Using the vomnibar
  o               Open URL, bookmark or history entry (Vomnibar.activate)
  O               Open URL, bookmark or history entry in a new tab (Vomnibar.activateInNewTab)
  b               Open a bookmark (Vomnibar.activateBookmarks)
  B               Open a bookmark in a new tab (Vomnibar.activateBookmarksInNewTab)
  T               Search through your open tabs (Vomnibar.activateTabSelection)
  gE              Edit the current URL and open in a new tab (Vomnibar.activateEditUrlInNewTab)
  Using find
  /               Enter find mode (enterFindMode)
  n               Cycle forward to the next find match (performFind)
  N               Cycle backward to the previous find match (performBackwardsFind)

**** Characters used for link hints

#+begin_src sh
  sadfjklewcmpgh
#+end_src

**** New tab url

https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0

**** Default search engine qfr

https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web

**** Backups

 - [2020-03-15 dim.]

   #+begin_src json
     {
         "settingsVersion": "1.66",
         "exclusionRules": [
             {
                 "pattern": "https?://mail.google.com/*",
                 "passKeys": ""
             }
         ],
         "filterLinkHints": false,
         "waitForEnterForFilteredHints": true,
         "hideHud": false,
         "keyMappings": "# When modify this, refer to the corresponding wiki page instead and update it too !!\n# See wiki page\n\nmap sq Vomnibar.activate keyword=q\nmap sQ Vomnibar.activateInNewTab keyword=q\nmap sg Vomnibar.activate keyword=g\nmap sG Vomnibar.activateInNewTab keyword=g\n\nmap <a-n> nextTab\nmap <a-p> previousTab\nmap d removeTab\nmap u restoreTab\n\nmap <c-e> Vomnibar.activateEditUrl\n# map <c-E> activateEditUrlInNewTab\n\nunmap ge\nunmap gE\nunmap x\nunmap X\nunmap J\nunmap gt\nunmap gT\nunmap K\nunmap J\nunmap <c-y>",
         "linkHintCharacters": "sadfjklewcmpgh",
         "linkHintNumbers": "0123456789",
         "newTabUrl": "https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0",
         "nextPatterns": "next,more,newer,>,›,→,»,≫,>>",
         "previousPatterns": "prev,previous,back,older,<,‹,←,«,≪,<<",
         "regexFindMode": false,
         "ignoreKeyboardLayout": false,
         "scrollStepSize": 60,
         "smoothScroll": true,
         "grabBackFocus": false,
         "searchEngines": "# When modify this, refer to the corresponding wiki page instead and update it too !!\n\n# ref: search wiki page\n# https://github.com/philc/vimium/wiki/Search-Engines\n\n# Default search engine qfr\n# https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web\n\n# New tab url\n# https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0\n\n# Qwant\n\nqen: https://www.qwant.com/?r=GB&sr=en&l=en_gb&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant en\nqfr: https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant fr\nqes: https://www.qwant.com/?r=ES&sr=es&l=es_es&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web Qwant es\n\n\nqlen: https://lite.qwant.com/?l=en&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite en\nqles: https://lite.qwant.com/?l=es&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite es\nqlfr: https://lite.qwant.com/?l=fr&?safesearch=0&suggests=true&s=0&b=1&a=1&q=%s&t=web QwantLite fr\n\n# ArchWiki\n\naw https://wiki.archlinux.org/index.php?search=%s ArchWiki\n\n# Wikipedia\n\nwen: https://en.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia en\nwes: https://es.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia es\nwfr: https://fr.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia es\n\n# Google\n\ngen: https://www.google.com/search?q=%s Google en\nges: https://www.google.es/search?q=%s Google es\ngfr: https://www.google.fr/search?q=%s Google fr\n\ngm: https://www.google.com/maps?q=%s Google maps\n# l: https://www.google.com/search?q=%s&btnI I'm feeling lucky...\n# l: https://www.google.com/search?btnI&q=%s I'm feeling lucky...\n\n# Youtube\n\ny: https://www.youtube.com/results?search_query=%s Youtube\n\n# DuckDuckGo\n\n# d: https://duckduckgo.com/?q=%s DuckDuckGo\nd: https://duckduckgo.com/?ia=about&q=%s DuckDuckGo\n\n# Amazon\n\n# az: https://www.amazon.com/s/?field-keywords=%s Amazon\naz: http://www.amazon.co.uk/s/?field-keywords=%s Amazon\n\n# Dictionary (Merriam-Webster).\n# dw: http://www.merriam-webster.com/dictionary/%s Merriam-Webster\n\n# b: https://www.bing.com/search?q=%s Bing",
         "searchUrl": "https://www.qwant.com/?r=FR&sr=fr&l=fr_fr&h=0&s=0&a=1&b=1&vt=0&hc=0&smartNews=0&smartSocial=0&theme=1&i=1&donation=0&q=%s&t=web",
         "userDefinedLinkHintCss": "div > .vimiumHintMarker {\n/* linkhint boxes */\nbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FFF785),\n  color-stop(100%,#FFC542));\nborder: 1px solid #E3BE23;\n}\n\ndiv > .vimiumHintMarker span {\n/* linkhint text */\ncolor: black;\nfont-weight: bold;\nfont-size: 12px;\n}\n\ndiv > .vimiumHintMarker > .matchingCharacter {\n}"
     }
   #+end_src

** Security

*** Tracking

https://support.mozilla.org/en-US/kb/tracking-protection-pbm?as=u

*** ExExCeptions

- https://addons.mozilla.org/fr/firefox/addon/exexceptions/
- http://www.dsfc.net/internet/tracking/un-bloqueur-publicitaire-integre-a-firefox/

* Searching

In menu bar:

 - Settings -> Search -> Search Engine -> Keyword
 - “Add a keyword for this search ...” to bookmarks
 - Qwant qwicks
