---
format: Org
categories: software pentadactyl
toc: yes
title: pentadactyl
content: pentadactyl configuration file
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

 - [[#notes][Notes]]
 - [[#build-from-source-and-install][Build from source and install]]
 - [[#code][Code]]
   - [[#header][Header]]
   - [[#plugins][Plugins]]
   - [[#toggling][Toggling]]
   - [[#][.]]
   - [[#colorscheme][Colorscheme]]
   - [[#browser-settings][Browser settings]]
     - [[#iframes][Iframes]]
     - [[#ssl][SSL]]
   - [[#search-engine][Search Engine]]
   - [[#commands][Commands]]
     - [[#transport][Transport]]
     - [[#boncoin][Boncoin]]
     - [[#sites][Sites]]
     - [[#ciemat][Ciemat]]
     - [[#feedly][Feedly]]
     - [[#open-street-maps][Open Street Maps]]
     - [[#autistici][Autistici]]
     - [[#mendeley][Mendeley]]
     - [[#diaspora][Diaspora]]
     - [[#lobsters][Lobsters]]
     - [[#journal-du-pirate][Journal du Pirate]]
     - [[#wallabag][Wallabag]]
       - [[#get-to-home-page-a-w-h][Get to home page A-w h]]
       - [[#add-new-item--a-w-a][Add New item  A-w a]]
     - [[#pocket][Pocket]]
       - [[#get-to-home-page-a-p-h][Get to home page A-p h]]
   - [[#pinboard][Pinboard]]
       - [[#get-to-home-page-a-p-h-1][Get to home page A-p h]]
       - [[#get-to-oldest--a-p-o][Get to oldest  A-p o]]
       - [[#add-new-item--a-p-a][Add New item  A-p a]]
       - [[#get-to-raincy-tag-a-p-r][Get to Raincy tag A-p r]]
       - [[#get-to-random-unread-a-p-r][Get to random unread A-p r]]
       - [[#get-to-read-later][Get to Read Later]]
       - [[#get-to-same-page][Get to Same Page]]
       - [[#get-to-a-p-t][Get to A-p t]]
   - [[#noscript][NoScript]]
   - [[#readability][Readability]]
   - [[#reader-mode][Reader Mode]]
   - [[#tabbing][Tabbing]]
   - [[#flash][Flash]]
   - [[#browsing][Browsing]]
   - [[#general-settings][General Settings]]
 - [[#references][References]]

* Notes

- Comments are preceded by a single "

- To get help :: ":help guioptions"

- To reload the config file :: ":rehash"

* Build from source and install

All what is necessary to build from source is [[http://5digits.org/coding][here]].

To sum up:

- git clone https://github.com/5digits/dactyl
- make -C pentadactyl xpi

This resulting XPI will be placed in the downloads/ folder.

I use the =aur/pentadactyl-hg= package to automatize the process.

* Code

** Header

#+begin_src sh :tangle pentadactylrc

  " Pentadactylrc configuration file

#+end_src

** Plugins

Load the plugins

#+begin_src sh :tangle pentadactylrc
  loadplugins '\.(js|penta)$'
  group user
#+end_src

** Toggling

Using "C-x t"

Toolbars

#+begin_src sh :tangle pentadactylrc
  nmap <C-x><t><b> -e :toolbartoggle Bookmarks Toolbar
  nmap <C-x><t><n> -e :toolbartoggle Navigation Toolbar
  nmap <C-x><t><m> -e :toolbartoggle Menu Bar
  nmap <C-x><t><a> -e :toolbartoggle Add-on Bar
  " map -count -modes=n,v <C-x><t><j> :toolbartoggle<Space>Add-on Bar<Return>
#+end_src

Downloads

#+begin_src sh :tangle pentadactylrc
  " show downloads
  nmap <C-x><t><d> -e :downloads
#+end_src

Full screen

#+begin_src sh :tangle pentadactylrc
  " Full-screen
  command! -description='set full-screen' myfs set fs
  command! -description='unset full-screen' mynofs set nofs
  map -count -modes=n,v <C-x><t><f> <count>:myfs<Return>
  map -count -modes=n,v <C-x><t><u> <count>:mynofs<Return>
#+end_src

** .

#+begin_src sh :tangle pentadactylrc
  " Hide the status bar when using fullscreen
  au fullscreen on set go-=s
  au fullscreen off set go+=s
#+end_src

** Colorscheme

#+begin_src sh :tangle pentadactylrc
  colorscheme zenburn
#+end_src

** Browser settings

Set about:config params.

*** Iframes

Disable iframes

#+begin_src sh :tangle pentadactylrc
  set! browser.frames.enabled=false
#+end_src

*** SSL

Require websites preform safe ssl negotiation

#+begin_src sh :tangle pentadactylrc
  set! security.ssl.require_safe_negotiation=true
  set! security.ssl.treat_unsafe_negotiation_as_broken=true
#+end_src

** Search Engine

Use duckduckgo to search by default

#+begin_src sh :tangle pentadactylrc
  map -builtin -modes=n s :open duckduckgo-next !g
  map -builtin -modes=n S :tabopen duckduckgo-next !g
#+end_src

** Commands

*** Transport

#+begin_src sh :tangle pentadactylrc
  command! -description='idf-transilien'     rncy-idf-transilien     open https://www.transilien.com/contents/fr/_Docs-PDF/Le-reseau/reseau_iledefrance.pdf
  command! -description='seine-est-ratp'     rncy-seine-est-ratp     open http://www.ratp.fr/informer/pdf/orienter/f_plan.php?fm=pdf&loc=secteur&nompdf=secteur8
  command! -description='seine-ouest-ratp'   rncy-seine-ouest-ratp   open http://www.ratp.fr/informer/pdf/orienter/f_plan.php?fm=pdf&loc=secteur&nompdf=secteur9
  command! -description='ratp'               rncy-ratp               open http://www.ratp.fr/fr/ratp/c_20559/consultez-l-ensemble-des-plans/
  command! -description='seine-all-transdev' rncy-seine-all-transdev open http://www.transdev-idf.com/Plan-plan_du_reseau_de_seine_saint_denis-tra-seine_saint_denis_293
  command! -description='noctilien'          rncy-noctilien-sncf     open http://www.transilien.com/static/noctilien
  command! -description='citymapper'         rncy-citymapper         tabopen https://citymapper.com/paris/d/rond-point-thiers-sw
  command! -description='prochain-train'     rncy-prochain-train     tabopen http://www.transilien.mobi/train/result?idOrigin=RVM&idDest=
#+end_src

*** Boncoin

#+begin_src sh :tangle pentadactylrc
  command! -description='boncoin-raicny'     boncoin-raincy     tabopen http://www.leboncoin.fr/annonces/offres/ile_de_france/?f=a&th=1&location=le%20raincy
  command! -description='boncoin-paris'      boncoin-paris      tabopen http://www.leboncoin.fr/annonces/offres/ile_de_france/?f=a&th=1&location=le%20paris
#+end_src

*** Sites

#+begin_src sh :tangle pentadactylrc
  " command! -description='arkof wallabag'  arkos-wallabag   tabopen https://csantosb.ddns.net:55591
  " command! -description='arkos genesis'   arkos-genesis    tabopen https://csantosb.ddns.net:55547
  " command! -description='arkos syncthing' arkos-syncthing  tabopen https://csantosb.ddns.net:8081

  command! -description='emacs subreddit'        emacs-reddit tabopen https://www.reddit.com/r/emacs/
  command! -description='emacs at stackexchange' emacs-se tabopen https://emacs.stackexchange.com/

  command! -description='owncloud'      owncloud   tabopen https://owncloud.csantosb.pw
  command! -description='my cv'         cv tabopen http://cv.csantosb.pw
  command! -description='miniflux'      miniflux tabopen http://csantosb.pw:81/miniflux/?action=login
  command! -description='blog pw'       blog-pw tabopen http://blog.csantosb.pw
  command! -description='blog github'   blog-github tabopen https://csantosb.github.io
  command! -description='blog noblogs'  blog-noblogs tabopen https://csantosb.noblogs.org
  command! -description='mailpile'      mail tabopen http://mail.csantosb.pw
  command! -description='gogs'          gogs tabopen http://gogs.csantosb.pw

  command! -description='gitit wiki online'  gitit-online tabopen http://gitit.csantosb.pw
  command! -description='gitit wiki'         gitit tabopen http://localhost:5001
  command! -description='gitit wiki ciemat'  gitit-ciemat tabopen http://localhost:5002
  command! -description='gitit wiki apc'     gitit-apc tabopen http://localhost:5003

  command! -description='Git Web'         gitweb tabopen http://localhost:51876/gitweb
  command! -description='syncthing local' syncthing-local tabopen http://127.0.0.1:61342
  command! -description='syncthing vultr' syncthing-pw tabopen http://syncthinig.csantosb.pw
  command! -description='yacy'            yacy tabopen https://188.226.230.93:8090
#+end_src

*** Ciemat

#+begin_src sh :tangle pentadactylrc
  command! -description='ciemat webmail' ciemat set nst=https://webmail.ciemat.es/Exchange | tabopen https://webmail.ciemat.es/Exchange
#+end_src

*** Feedly

#+begin_src sh :tangle pentadactylrc
  command! -description='Subscribe To current Rss Feed with Feedly' feedly set nst=http://feedly.com,https://feedly.com,feedly.com | open javascript:void(d=document);void(el=d.getElementsByTagName('link'));void(g=false);for(i=0;i<el.length;i++){if(el[i].getAttribute('rel').indexOf('alternate')!=-1){ty=el[i].getAttribute('type');if(ty.indexOf('application/rss+xml')!=-1||ty.indexOf('text/xml')!=-1){g=true;h=el[i].getAttribute('href');void(location.href='http://www.feedly.com/home#subscription/feed/'+h);}}};if(!g){window.alert('Could%20not%20find%20the%20RSS%20Feed');};
  " set passkeys+=feedly.com:gi,gm,ga,gg,gl,/,r,j,k,n,p,o,v,m,x,s,t,b
#+end_src

*** Open Street Maps

#+begin_src sh :tangle pentadactylrc
  command osmaps -description "Open Street Map" -javascript <<EOF
           dactyl.execute(":set nst=https://www.openstreetmap.org,http://openstreetmap.org,https://openstreetmap.org,openstreetmap.org");
           dactyl.open("https://www.openstreetmap.org/#map=13/40.3881/-3.5739");
  EOF
#+end_src

*** Autistici

#+begin_src sh :tangle pentadactylrc
  " ORG-MODE"
  " Create a keystroke to send current page, along with title and part of contents to org-capture template"
  " use org-protocol for this"
  " and thread 'passing url to a shell command?' in gmane"
  " :js CommandExMode().open("!urxvt -e cclive " + content.location)"
  " :js io.run("urxvt", ["-e", "cclive", buffer.URL])"

  " link autistici org"

  command! -description='Autistici Link Home' lah tabopen https://link.autistici.org/bookmarks.php/csantosb
  map -count -modes=n,v <A-l>h :set<Space>nst=<Tab><Return>:lah<Return>

  command! -description='Add to link.autistici.org' laa open javascript:x=document;a=encodeURIComponent(x.location.href);t=encodeURIComponent(x.title);d=encodeURIComponent(window.getSelection());location.href='https://link.autistici.org/bookmarks.php/csantosb?action=add&address='+a+'&title='+t+'&description='+d;void%200;

  map -count -modes=n,v <A-l>a <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>:laa<Return>
#+end_src

*** Mendeley

#+begin_src sh :tangle pentadactylrc
  command! -description='Bookmarklet: Save to Mendeley' mend open javascript:document.getElementsByTagName('body')[0].appendChild(document.createElement('script')).setAttribute('src','https://www.mendeley.com/minified/bookmarklet.js');
#+end_src

*** Diaspora

#+begin_src sh :tangle pentadactylrc
  command! -description='Bookmarklet: Save to Diaspora' dip open javascript:(function(){f='http://sharetodiaspora.github.io/?url='+encodeURIComponent(window.location.href)+'&title='+encodeURIComponent(document.title)+'&notes='+encodeURIComponent(''+(window.getSelection?window.getSelection():document.getSelection?document.getSelection():document.selection.createRange().text))+'&v=1&';a=function(){if(!window.open(f+'noui=1&jump=doclose','diasporav1','location=yes,links=no,scrollbars=no,toolbar=no,width=620,height=550'))location.href=f+'jump=yes'};if(/Firefox/.test(navigator.userAgent)){setTimeout(a,0)}else{a()}})()
  map -count -modes=n,v <A-d>a <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>:dip<Return>
#+end_src

*** Lobsters

#+begin_src sh :tangle pentadactylrc
  command! -description='Bookmarklet: Save to Lobste.rs' lob open javascript:window.location="https://lobste.rs/stories/new?url="+encodeURIComponent(document.location)+"&title="+encodeURIComponent(document.title)
  map -count -modes=n,v <A-l>a <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>:lob<Return>
#+end_src

*** Journal du Pirate

#+begin_src sh :tangle pentadactylrc
  command! -description='Send to JdP' jdp open javascript:window.location=%22http://infos.mytux.fr/stories/new?url=%22+encodeURIComponent(document.location)+%22&title=%22+encodeURIComponent(document.title)
  map -count -modes=n,v <A-l>j :jdp<Return>
#+end_src

*** Wallabag

**** Get to home page A-w h

#+begin_src sh :tangle pentadactylrc
  command! -description='Wallabag Home' walh tabopen http://www.framabag.org/u/csantosb/
  map -count -modes=n,v <A-w>h :walh<Return>
#+end_src

**** Add New item  A-w a

Based on the bookmarklet to be found con the config page of framabag.

#+begin_src sh :tangle pentadactylrc
  command! -description='Save to Wallabag' wala open javascript:if(top['bookmarklet-url@wallabag.org']){top['bookmarklet-url@wallabag.org'];}else{(function(){var%20url%20=%20location.href%20||%20url;window.open('http://www.framabag.org/u/csantosb/?action=add&autoclose=true&url='%20+%20btoa(url),'_blank');})();void(0);}
  map -count -modes=n,v <A-w>a :wala<Return>
#+end_src

*** Pocket

**** Get to home page A-p h

#+begin_src sh :tangle pentadactylrc
  command! -description='Save to Pocket' poca open javascript:(function(){var%20e=function(t,n,r,i,s){var%20o=[4238522,1683657,4365113,5659626,6795523,1815916,4358038,5281505,6313453,5043809];var%20i=i||0,u=0,n=n||[],r=r||0,s=s||0;var%20a={'a':97,'b':98,'c':99,'d':100,'e':101,'f':102,'g':103,'h':104,'i':105,'j':106,'k':107,'l':108,'m':109,'n':110,'o':111,'p':112,'q':113,'r':114,'s':115,'t':116,'u':117,'v':118,'w':119,'x':120,'y':121,'z':122,'A':65,'B':66,'C':67,'D':68,'E':69,'F':70,'G':71,'H':72,'I':73,'J':74,'K':75,'L':76,'M':77,'N':78,'O':79,'P':80,'Q':81,'R':82,'S':83,'T':84,'U':85,'V':86,'W':87,'X':88,'Y':89,'Z':90,'0':48,'1':49,'2':50,'3':51,'4':52,'5':53,'6':54,'7':55,'8':56,'9':57,'\/':47,':':58,'?':63,'=':61,'-':45,'_':95,'&':38,'$':36,'!':33,'.':46};if(!s||s==0){t=o[0]+t}for(var%20f=0;f<t.length;f++){var%20l=function(e,t){return%20a[e[t]]?a[e[t]]:e.charCodeAt(t)}(t,f);if(!l*1)l=3;var%20c=l*(o[i]+l*o[u%o.length]);n[r]=(n[r]?n[r]+c:c)+s+u;var%20p=c%(50*1);if(n[p]){var%20d=n[r];n[r]=n[p];n[p]=d}u+=c;r=r==50?0:r+1;i=i==o.length-1?0:i+1}if(s==286){var%20v='';for(var%20f=0;f<n.length;f++){v+=String.fromCharCode(n[f]%(25*1)+97)}o=function(){};return%20v+'cc98094eea'}else{return%20e(u+'',n,r,i,s+1)}};var%20t=document,n=t.location.href,r=t.title;var%20i=e(n);var%20s=t.createElement('script');s.type='text/javascript';s.src='https://getpocket.com/b/r4.js?h='+i+'&u='+encodeURIComponent(n)+'&t='+encodeURIComponent(r);e=i=function(){};var%20o=t.getElementsByTagName('head')[0]||t.documentElement;o.appendChild(s)})()
  " map -count -modes=n,v <A-o>a <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>:pocket<Return>
  " map -count -modes=n,v <A-o>a <count>:set<Space>script<Space>script<Return>:reload<Return>:pocket<Return>
  map -count -modes=n,v <A-o>a :poca<Return>
#+end_src

#+begin_src sh :tangle pentadactylrc
  command! -description='Pocket Home' poch tabopen https://getpocket.com/a/queue/
  map -count -modes=n,v <A-o>h :poch<Return>
#+end_src

** Pinboard

**** Get to home page A-p h

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard Home' pbh tabopen https://pinboard.in/u:csantosb
  map -count -modes=n,v <A-p>h :set<Space>nst=<Tab><Return>:pbh<Return>
#+end_src

**** Get to oldest  A-p o

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard Oldest' pbo open https://pinboard.in/oldest/
  map -count -modes=n,v <A-p>o :set<Space>nst=<Tab><Return>:pbo<Return>
#+end_src

**** Add New item  A-p a

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard PoUp' pbp open javascript:q=location.href;if(document.getSelection){d=document.getSelection();}else{d='';};p=document.title;void(open('https://pinboard.in/add?url='+encodeURIComponent(q)+'&description='+encodeURIComponent(d)+'&title='+encodeURIComponent(p),'Pinboard','toolbar=no,width=700,height=350'));
  map -count -modes=n,v <A-p>a :pbp<Return>
#+end_src

**** Get to Raincy tag A-p r

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard Raincy Tag' pbr tabopen https://pinboard.in/u:csantosb/t:raincy
  map -count -modes=n,v <A-p>r :set<Space>nst=<Tab><Return>:pbo<Return>
  " map -count -modes=n,v <A-p>a <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>:pbp<Return>
#+end_src

**** Get to random unread A-p r

# +begin_src sh :tangle pentadactylrc
  " nst=<Tab><Return>:pbp<Return>
  command! -description='Pinboard Random' pbr open https://pinboard.in/random/?type=unread
  map -count -modes=n,v <A-p>r :set<Space>nst=<Tab><Return>:pbr<Return>
# +end_src

**** Get to Read Later

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard Read Later' pbrl open javascript:q=location.href;p=document.title;void(t=open('https://pinboard.in/add?later=yes&noui=yes&jump=close&url='+encodeURIComponent(q)+'&title='+encodeURIComponent(p),'Pinboard','toolbar=no,width=100,height=100'));t.blur();
#+end_src

**** Get to Same Page

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard Same Page' pbs open javascript:if(document.getSelection){s=document.getSelection();}else{s='';};document.location='https://pinboard.in/add?next=same&url='+encodeURIComponent(location.href)+'&description='+encodeURIComponent(s)+'&title='+encodeURIComponent(document.title)
#+end_src

**** Get to A-p t

#+begin_src sh :tangle pentadactylrc
  command! -description='Pinboard PopUp with Tags' pbt open javascript:q=location.href;if(document.getSelection){d=document.getSelection();}else{d='';};p=document.title;void(open('https://pinboard.in/add?showtags=yes&url='+encodeURIComponent(q)+'&description='+encodeURIComponent(d)+'&title='+encodeURIComponent(p),'Pinboard','toolbar=no,scrollbars=yes,width=750,height=700'));
#+end_src

** NoScript

disable

#+begin_src sh :tangle pentadactylrc
  " map -count -modes=n,v <A-s>d <count>:set<Space>script<Space>noscript<Return>:reload<Return>
#+end_src

enable

#+begin_src sh :tangle pentadactylrc
  " map -count -modes=n,v <A-s>e <count>:set<Space>script<Space>script<Return>:reload<Return>
#+end_src

temporally enable

#+begin_src sh :tangle pentadactylrc
  " map -count -modes=n,v <A-s>t <count>:set<Space>nst!=<A-Tab><Return>:reload<Return>
#+end_src

add to enabled sites

#+begin_src sh :tangle pentadactylrc
  " map -count -modes=n,v <A-s>s <count>:set<Space>nss!=<A-Tab><Return>:reload<Return>
#+end_src

#+begin_src sh :tangle pentadactylrc
  " set noscript-sites=https://www.autistici.org,http://readability.com,https://readability.com,readability.com,http://feedly.com,https://feedly.com,feedly.com,http://pinboard.in,https://pinboard.in,pinboard.in,https://noblogs.org,https://www.inventati.org,http://lobste.rs,https://lobste.rs,lobste.rs,http://duckduckgo.com,https://duckduckgo.com,duckduckgo.com,http://diaspora-fr.org,https://diaspora-fr.org,diaspora-fr.org,http://framabag.org,https://framabag.org,framabag.org,http://readability.com,https://readability.com,readability.com,http://getpocket.com,https://getpocket.com,getpocket.com,http://github.com,https://github.com,github.com,https://188.226.230.93
  " set noscript-untrusted=
  " disable
  " disable by default
  " set script noscript
  " list of disabled objects
  " set noscript-forbid=
  " flash,frame,iframe,fonts,java,media,placeholder,plugins,silverlight,xslt,refresh,collapse,webbug
#+end_src

** Readability

Bound to Alt-r

- Read

  #+begin_src sh :tangle pentadactylrc
    command! -description='Bookmarklet: Read Later with Readability' ra open javascript:(%0Arr%28function%28%29%7Bwindow.baseUrl%3D%27//www.readability.com%27%3Bwindow.readabilityToken%3D%27pv2VM8cp8sunwrBUKWxyP9MYNN6qh2WTTz2vVPM9%27%3Bvar%20s%3Ddocument.createElement%28%27script%27%29%3Bs.setAttribute%28%27type%27%2C%27text/javascript%27%29%3Bs.setAttribute%28%27charset%27%2C%27UTF-8%27%29%3Bs.setAttribute%28%27src%27%2CbaseUrl%2B%27/bookmarklet/save.js%27%29%3Bdocument.documentElement.appendChild%28s%29%3B%7D%29%28%29)
    map -count -modes=n,v <A-r>j :set<Space>nst!=<A-Tab><Return>:reload<Return><wait-for-page-load>:rl<Return>
    map -count -modes=n,v <A-r>a :ra<Return>
  #+end_src

- Read now current page.

  #+begin_src sh :tangle pentadactylrc
    command! -description='Bookmarklet: Read Now with Readability' rn open javascript:(%0A%28function%28%29%7Bwindow.baseUrl%3D%27//www.readability.com%27%3Bwindow.readabilityToken%3D%27pv2VM8cp8sunwrBUKWxyP9MYNN6qh2WTTz2vVPM9%27%3Bvar%20s%3Ddocument.createElement%28%27script%27%29%3Bs.setAttribute%28%27type%27%2C%27text/javascript%27%29%3Bs.setAttribute%28%27charset%27%2C%27UTF-8%27%29%3Bs.setAttribute%28%27src%27%2CbaseUrl%2B%27/bookmarklet/read.js%27%29%3Bdocument.documentElement.appendChild%28s%29%3B%7D%29%28%29)
    map -count -modes=n,v <A-r>n :rn<Return>
    map -count -modes=n,v <A-r>m :set<Space>nst!=<A-Tab><Return>:reload<Return><wait-for-page-load>:rn<Return>
  #+end_src

** Reader Mode

#+begin_src sh :tangle pentadactylrc
  map gr -js let r = document.getElementById('reader-mode-button'); if (!r.hidden) r.click()
#+end_src

** Tabbing

#+begin_src sh :tangle pentadactylrc
  " nmap <A-h> -e :tabprev
  " nmap <A-l> -e :tabnext
  " nmap <A-S-h> -e :tabmove -1
  " nmap <A-S-l> -e :tabmove +1
  :nmap -builtin <C-n> gt
  :nmap -builtin <C-p> gT
  :nmap -builtin <h> gh
#+end_src

** Flash

#+begin_src sh :tangle pentadactylrc
  map -count -modes=n,v <A-f>t <count>:flashtoggle<Return>
  map -count -modes=n,v <A-f>n <count>:flashstop<Return>
  map -count -modes=n,v <A-f>f <count>:flashplay<Return>
#+end_src

** Browsing

#+begin_src sh :tangle pentadactylrc
  " map -count -modes=n,v <A-+> <count>zI
  " map -count -modes=n,v <A--> <count>zO

  map -count -modes=n,v -builtin -arg <C-m>t :tabmove <arg><CR>
  map -count -modes=n,v -builtin > :tabmove! +1<CR>
  map -count -modes=n,v -builtin < :tabmove! -1<CR>

  map -count -modes=n,v <C-+> <count>ZI
  map -count -modes=n,v <C--> <count>ZO

  map -count -modes=n,v <+> <count>zi
  map -count -modes=n,v <-> <count>zo

  map -count -modes=n,v j -builtin <count>2j
  map -count -modes=n,v k -builtin <count>2k
  map -count -modes=n <C-x><C-c> <count>:quitall<Return>
  map -count -modes=n <C-x>q <count>:quit<Return>
  map -count -modes=n <C-x>u <count>:undo<Return>
  map -count -modes=n,v <C-s> <count>/
  map -count -modes=n,v <C-r> <count>?
  map -count -modes=n v -builtin <count>:set<Space>nst!=<A-Tab><Return>
  map -count -modes=n v -builtin <count>:set<Space>nss!=<A-Tab><Return>
#+end_src

** General Settings

Configure the command line and the look

Hint StackExchange voting buttons

#+begin_src sh :tangle pentadactylrc
  set hinttags+=a[class]
#+end_src

#+begin_src sh :tangle pentadactylrc
  set hinttags+=a[class]
  set maxitems=20
  set showtabline=multitab
  set showstatuslinks=command
  set defsearch=qwant
  " set editor='urxvt -e /home/csantos/Dropbox/myscripts/tmux_emacs_firefox'
  set editor='urxvt -e /home/csantos/Scripts/em'
  " set editor='urxvt -e zsh -c "zsh -i -c tm-emacs-firefox"'
  " set editor='urxvt -e emacsclient -c'
  set guioptions=C
  set hintkeys=fghjkvbnmrtyui
  set hintinputs=label,value
  set hintmatching=contains
  " only follow hints when press enter
  set followhints=0
  " set mapleader="\"
  hi -a Hint font-size: 9pt !important;
  " set scrollsteps=10
  " set scrolltime=2
  set showstatuslinks=command
  " find as you type
  set incfind
  " highlight all search matches, not just the first
  set hlfind
  set findcase=smart
  :map -b j 2j

" hide close buttons on tabs (less mouse temptation)
set! browser.tabs.closeButtons=2

  " vim: set ft=pentadactyl:
#+end_src

* References

- http://pentablg.blogspot.com.es/
- [[https://github.com/joedicastro/dotfiles/blob/master/pentadactyl/pentadactylrc][joe di castro config file]]
- http://www.jnanam.net/pentadactylemacs/.pentadactylrc
- http://nakkaya.com/2014/01/26/pentadactyl-configuration/
- https://github.com/helmuthdu/pentadactyl/blob/master/pentadactylrc
