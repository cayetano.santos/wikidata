---
format: Org
categories: software
toc: yes
title: ranger
content: ranger
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#use][Use]]
- [[#config][Config]]
  - [[#utilities][Utilities]]
- [[#references][References]]

* Use

? = View ranger man page
1? = Keybindings help
2? = Command help
3? = Settings help
R = Reload current directory
Q = Quit

* Config

- system wide :: /usr/lib/python3.6/site-packages/ranger
- user :: /home/csantos/.config/ranger

** Utilities

 - rifle :: global and local
 - commands :: global and local
 - rc.conf :: global and local
 - scope.sh :: local and [[file:/usr/lib/python3.6/site-packages/ranger/data/scope.sh][file:/usr/lib/python3.6/site-packages/ranger/data/scope.sh]]

* References

 - [[https://ranger.github.io/][Web site]]
 - [[https://github.com/hut/ranger/wiki][github wiki]]
 - [[http://joedicastro.com/productividad-linux-ranger.html][Productividad & Linux: Ranger]]
 - https://wiki.archlinux.org/index.php/Ranger
