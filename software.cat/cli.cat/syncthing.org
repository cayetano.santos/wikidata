---
format: Org
categories: software cli
toc: yes
title: Syncthing
content: Syncthing related
...

* Config file
:PROPERTIES:
:header-args:    :tangle /home/csantos/.config/syncthing/config.xml
:END:

#+begin_src xml :padline no
  <!-- ####################################################################### -->
  <!-- #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! # -->
  <!-- ####################################################################### -->
#+end_src

#+begin_src xml
  <configuration version="21">
#+end_src

** Folders

#+begin_src xml
      <folder id="documents" label="" path="~/Documents/" type="readwrite" rescanIntervalS="65" ignorePerms="false" autoNormalize="true">
          <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" introducedBy=""></device>
          <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" introducedBy=""></device>
          <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" introducedBy=""></device>
          <minDiskFree unit="%">1</minDiskFree>
          <versioning type="simple">
              <param key="keep" val="10"></param>
          </versioning>
          <copiers>1</copiers>
          <pullers>16</pullers>
          <hashers>0</hashers>
          <order>random</order>
          <ignoreDelete>false</ignoreDelete>
          <scanProgressIntervalS>0</scanProgressIntervalS>
          <pullerSleepS>0</pullerSleepS>
          <pullerPauseS>0</pullerPauseS>
          <maxConflicts>-1</maxConflicts>
          <disableSparseFiles>false</disableSparseFiles>
          <disableTempIndexes>false</disableTempIndexes>
          <fsync>true</fsync>
          <paused>false</paused>
          <weakHashThresholdPct>25</weakHashThresholdPct>
      </folder>
#+end_src

#+begin_src xml
      <folder id="maildir-encfs" label="" path="~/.maildir/" type="readwrite" rescanIntervalS="60" ignorePerms="false" autoNormalize="true">
          <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" introducedBy=""></device>
          <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" introducedBy=""></device>
          <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" introducedBy=""></device>
          <minDiskFree unit="%">1</minDiskFree>
          <versioning></versioning>
          <copiers>1</copiers>
          <pullers>16</pullers>
          <hashers>0</hashers>
          <order>random</order>
          <ignoreDelete>false</ignoreDelete>
          <scanProgressIntervalS>0</scanProgressIntervalS>
          <pullerSleepS>0</pullerSleepS>
          <pullerPauseS>0</pullerPauseS>
          <maxConflicts>-1</maxConflicts>
          <disableSparseFiles>false</disableSparseFiles>
          <disableTempIndexes>false</disableTempIndexes>
          <fsync>true</fsync>
          <paused>false</paused>
          <weakHashThresholdPct>25</weakHashThresholdPct>
      </folder>
#+end_src

#+begin_src xml
      <folder id="dropbox" label="" path="~/Dropbox/" type="readwrite" rescanIntervalS="55" ignorePerms="false" autoNormalize="true">
          <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" introducedBy=""></device>
          <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" introducedBy=""></device>
          <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" introducedBy=""></device>
          <minDiskFree unit="%">1</minDiskFree>
          <versioning type="simple">
              <param key="keep" val="10"></param>
          </versioning>
          <copiers>1</copiers>
          <pullers>16</pullers>
          <hashers>0</hashers>
          <order>random</order>
          <ignoreDelete>false</ignoreDelete>
          <scanProgressIntervalS>0</scanProgressIntervalS>
          <pullerSleepS>0</pullerSleepS>
          <pullerPauseS>0</pullerPauseS>
          <maxConflicts>-1</maxConflicts>
          <disableSparseFiles>false</disableSparseFiles>
          <disableTempIndexes>false</disableTempIndexes>
          <fsync>true</fsync>
          <paused>false</paused>
          <weakHashThresholdPct>25</weakHashThresholdPct>
      </folder>
#+end_src

#+begin_src xml
      <folder id="pictures" label="" path="~/Pictures/" type="readwrite" rescanIntervalS="55" ignorePerms="false" autoNormalize="true">
          <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" introducedBy=""></device>
          <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" introducedBy=""></device>
          <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" introducedBy=""></device>
          <minDiskFree unit="%">1</minDiskFree>
          <versioning type="simple">
              <param key="keep" val="10"></param>
          </versioning>
          <copiers>1</copiers>
          <pullers>16</pullers>
          <hashers>0</hashers>
          <order>random</order>
          <ignoreDelete>false</ignoreDelete>
          <scanProgressIntervalS>0</scanProgressIntervalS>
          <pullerSleepS>0</pullerSleepS>
          <pullerPauseS>0</pullerPauseS>
          <maxConflicts>-1</maxConflicts>
          <disableSparseFiles>false</disableSparseFiles>
          <disableTempIndexes>false</disableTempIndexes>
          <fsync>true</fsync>
          <paused>false</paused>
          <weakHashThresholdPct>25</weakHashThresholdPct>
      </folder>
#+end_src

#+begin_src xml
      <folder id="orgagenda" label="" path="~/OrgAgenda/" type="readwrite" rescanIntervalS="650" ignorePerms="false" autoNormalize="true">
          <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" introducedBy=""></device>
          <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" introducedBy=""></device>
          <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" introducedBy=""></device>
          <device id="WPIJTXX-LS22GOI-G6V5QVI-DVZ5YHH-5Z5ARTN-KKFCZEN-NP7R4XM-PDYWWAH" introducedBy=""></device>
          <minDiskFree unit="%">1</minDiskFree>
          <versioning type="simple">
              <param key="keep" val="10"></param>
          </versioning>
          <copiers>1</copiers>
          <pullers>16</pullers>
          <hashers>0</hashers>
          <order>random</order>
          <ignoreDelete>false</ignoreDelete>
          <scanProgressIntervalS>0</scanProgressIntervalS>
          <pullerSleepS>0</pullerSleepS>
          <pullerPauseS>0</pullerPauseS>
          <maxConflicts>-1</maxConflicts>
          <disableSparseFiles>false</disableSparseFiles>
          <disableTempIndexes>false</disableTempIndexes>
          <fsync>true</fsync>
          <paused>false</paused>
          <weakHashThresholdPct>25</weakHashThresholdPct>
      </folder>
#+end_src

** Devices

#+begin_src xml
  <!-- <device id="EKK2CHU-RBVELB3-AJWMW4J-MXCDI4G-ZQZYGX4-GZ6T32F-E6DA5VV-7WVPMQI" name="desktop.home.arch" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy=""> -->
  <!--     <address>tcp://89.141.20.163:22002</address> -->
  <!--     <paused>false</paused> -->
  <!-- </device> -->
  <device id="F4QTZOR-AVV5YAX-XAN47T4-5LNCERG-BV5CWQJ-FIZXAZ6-LQZZMMX-RSBWGAU" name="apcnb158" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
      <address>dynamic</address>
      <paused>false</paused>
  </device>
  <device id="H6U5B3F-HWBQS32-POEUWE7-7MZVMZH-PDUZZGI-IAJHUL2-C272WWC-PCHHNQR" name="vultr.csantosb" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
      <address>tcp://104.238.191.128:22000</address>
      <paused>false</paused>
  </device>
  <device id="WPIJTXX-LS22GOI-G6V5QVI-DVZ5YHH-5Z5ARTN-KKFCZEN-NP7R4XM-PDYWWAH" name="tablet" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
      <address>dynamic</address>
      <paused>false</paused>
  </device>
#+end_src

** Gui

#+begin_src xml
      <gui enabled="true" tls="false" debugging="false">
          <address>127.0.0.1:61342</address>
          <apikey>elxtdHRR0GFz7odRo8e13YdntsuWLepS</apikey>
          <theme>default</theme>
      </gui>
#+end_src

** Options

#+begin_src xml
      <options>
          <listenAddress>default</listenAddress>
          <globalAnnounceServer>default</globalAnnounceServer>
          <globalAnnounceEnabled>false</globalAnnounceEnabled>
          <localAnnounceEnabled>false</localAnnounceEnabled>
          <localAnnouncePort>21027</localAnnouncePort>
          <localAnnounceMCAddr>[ff12::8384]:21027</localAnnounceMCAddr>
          <maxSendKbps>0</maxSendKbps>
          <maxRecvKbps>0</maxRecvKbps>
          <reconnectionIntervalS>60</reconnectionIntervalS>
          <relaysEnabled>true</relaysEnabled>
          <relayReconnectIntervalM>10</relayReconnectIntervalM>
          <startBrowser>false</startBrowser>
          <natEnabled>false</natEnabled>
          <natLeaseMinutes>0</natLeaseMinutes>
          <natRenewalMinutes>30</natRenewalMinutes>
          <natTimeoutSeconds>10</natTimeoutSeconds>
          <urAccepted>-1</urAccepted>
          <urUniqueID></urUniqueID>
          <urURL>https://data.syncthing.net/newdata</urURL>
          <urPostInsecurely>false</urPostInsecurely>
          <urInitialDelayS>1800</urInitialDelayS>
          <restartOnWakeup>false</restartOnWakeup>
          <autoUpgradeIntervalH>0</autoUpgradeIntervalH>
          <upgradeToPreReleases>false</upgradeToPreReleases>
          <keepTemporariesH>24</keepTemporariesH>
          <cacheIgnoredFiles>false</cacheIgnoredFiles>
          <progressUpdateIntervalS>5</progressUpdateIntervalS>
          <limitBandwidthInLan>false</limitBandwidthInLan>
          <minHomeDiskFree unit="%">1</minHomeDiskFree>
          <releasesURL>https://upgrades.syncthing.net/meta.json</releasesURL>
          <overwriteRemoteDeviceNamesOnConnect>false</overwriteRemoteDeviceNamesOnConnect>
          <tempIndexMinBlocks>10</tempIndexMinBlocks>
          <trafficClass>0</trafficClass>
          <weakHashSelectionMethod>auto</weakHashSelectionMethod>
          <stunServer>default</stunServer>
          <stunKeepaliveSeconds>24</stunKeepaliveSeconds>
          <defaultKCPEnabled>false</defaultKCPEnabled>
          <kcpNoDelay>false</kcpNoDelay>
          <kcpUpdateIntervalMs>25</kcpUpdateIntervalMs>
          <kcpFastResend>false</kcpFastResend>
          <kcpCongestionControl>true</kcpCongestionControl>
          <kcpSendWindowSize>128</kcpSendWindowSize>
          <kcpReceiveWindowSize>128</kcpReceiveWindowSize>
          <defaultFolderPath>~</defaultFolderPath>
          <minHomeDiskFreePct>0</minHomeDiskFreePct>
      </options>
  </configuration>
#+end_src

* [[https://pulse-forum.ind.ie/t/firewalls-and-port-forwards/166][Firewalls and port forwards]]

- Port Forwards

  If you have a NAT router, the easiest way to get a working port forward is to make sure UPnP is
  enabled - syncthing will handle the rest.

  If this is not possible or desirable you should set up a port forward for port 22000/TCP, or the
  port set in the Sync Protocol Listen Address setting.

- Local Firewall. If your PC has a local firewall, you will need to open the following ports for
  incoming traffic:

    + Port 22000/TCP (or the actual listening port if you have changed the Sync Protocol Listen Address setting.)
    + Port 21025/UDP (for discovery broadcasts)
